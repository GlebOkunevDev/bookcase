package bookcase.grimald.com.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;

import bookcase.grimald.com.database.DatabaseHelper;
import bookcase.grimald.com.model.Book;
import bookcase.grimald.com.util.ShowBookFragmentUtil;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Service to remove or move to another category the marked books if the application have turned off or have closed
 * Created by user on 21.10.2016.
 */

public class ActionWithBookService extends Service{
    private String TAG = getClass().getSimpleName();

    public static final String SELECT_BOOK_ARRAY = "select_book_array";
    public static final String TYPE_OF_ACTION_WITH_BOOK_ARRAY = "type_of_action_with_book_array";
    public static final String NEW_BOOK_STATUS = "new_book_status";
    public static final int DELETE_ACTION = 1;

    private RealmResults<Book> mBookList;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate()");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        ArrayList<Book> selectBookArray = intent.getParcelableArrayListExtra(SELECT_BOOK_ARRAY);
        int action = intent.getIntExtra(TYPE_OF_ACTION_WITH_BOOK_ARRAY, 0);
        final int newBookStatus = intent.getIntExtra(NEW_BOOK_STATUS, -1);
        mBookList = DatabaseHelper.getInstance().getBookList();

        if(selectBookArray == null || mBookList == null) {
            stopSelf();
            return START_NOT_STICKY;
        }

        switch (action){
            case DELETE_ACTION: //Delete book
                for(int i = 0; i < selectBookArray.size(); i++) {
                    Log.d(TAG, "onStartCommand + fori + delete");
                    DatabaseHelper.getInstance().deleteBookFromDB(selectBookArray.get(i));
                    if(selectBookArray.get(i).getLittleImagePath() != null)
                        ShowBookFragmentUtil.startImageService(this, null, selectBookArray.get(i).getLittleImagePath(),
                                                                null, ImageWorkService.DELETE_IMAGE);
                }
                break;
            case 2: //Move book
                if(newBookStatus == -1) break;
                for(int i = 0; i < selectBookArray.size(); i++) {
                    Log.d(TAG, "onStartCommand + fori + move");
                    final Book book = selectBookArray.get(i);
                    DatabaseHelper.getInstance().getRealm().executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            book.setStatus(newBookStatus);
                            if(newBookStatus == 1) {
                                book.setEndReading(Calendar.getInstance().getTime());
                            }
                        }
                    });
                    DatabaseHelper.getInstance().createOrUpdateBookFromDB(book);
                }
                break;
        }

        stopSelf();
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        if(DatabaseHelper.getInstance().getRealm() != null && !DatabaseHelper.getInstance().getRealm().isClosed())
            DatabaseHelper.getInstance().getRealm().close();
    }
}
