package bookcase.grimald.com.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.IOException;

import bookcase.grimald.com.model.TempBook;
import bookcase.grimald.com.util.ImageUtil;

/**
 * IntentService for copy or delete images not in main thread
 * Created by Глеб on 07.12.2016.
 */

public class ImageWorkService extends IntentService {

    public static final String ACTION_WITH_IMAGE = "action_with_image";
    public static final String IMAGE_PATH = "image_path";
    public static final String BOOK_FOR_ACTION = "book_for_action";
    public static final String PATH_CATEGORY = "path_category";
    public static final int COPY_IMAGE = 1;
    public static final int DELETE_IMAGE = 2;
    public static final String LITTLE_IMAGE_BOOK_COVER = "little_image_book_cover";
    public static final String GET_IMAGE_NAME_INTENT_FILTER = "get_image_name_intent_filter";

    public ImageWorkService() {
        super("ImageWorkService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if(intent == null)  return;

        int actionWithImage = intent.getIntExtra(ACTION_WITH_IMAGE, 0);
        String imagePath = intent.getStringExtra(IMAGE_PATH);
        TempBook book = intent.getParcelableExtra(BOOK_FOR_ACTION);
        boolean isRealPath = intent.getBooleanExtra(PATH_CATEGORY, false);

        switch (actionWithImage){
            case COPY_IMAGE:
                try {
                    ImageUtil.copyFile(imagePath, this, book, isRealPath);

                    Log.d("COPY_IMAGE", ImageUtil.getLittleImageFileName());
                    Intent broadcastIntent = new Intent(GET_IMAGE_NAME_INTENT_FILTER);
                    broadcastIntent.putExtra(LITTLE_IMAGE_BOOK_COVER, ImageUtil.getLittleImageFileName());
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case DELETE_IMAGE:
                Log.d("DELETE_IMAGE", imagePath);
                ImageUtil.deleteFile(imagePath);
                break;
            default:
                break;
        }
    }
}
