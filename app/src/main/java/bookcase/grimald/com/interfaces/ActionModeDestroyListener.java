package bookcase.grimald.com.interfaces;

import android.support.v7.view.ActionMode;

/**
 * Created by user on 12.10.2016.
 */

public interface ActionModeDestroyListener {
    void onDestroyActionMode(ActionMode mode);
}
