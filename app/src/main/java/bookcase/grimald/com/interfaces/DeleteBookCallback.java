package bookcase.grimald.com.interfaces;

/**
 * Created by user on 14.10.2016.
 */

public interface DeleteBookCallback {
    void onDeleteBookCallback(int status, int size);
}
