package bookcase.grimald.com.interfaces;

import android.support.v7.view.ActionMode;

import bookcase.grimald.com.adapter.CustomRecyclerViewAdapter;

/**
 * Interface for listen when user want to share books to another app
 * Created by user on 15.11.2016.
 */

public interface ActionModeShareListener {
    void onActionModeShareBooks(CustomRecyclerViewAdapter adapter, ActionMode mode);
}
