package bookcase.grimald.com.interfaces;

import android.support.v7.view.ActionMode;

import bookcase.grimald.com.adapter.CustomRecyclerViewAdapter;

/**
 * Interface for listen when user want to move books from one list to another
 * Created by user on 31.10.2016.
 */

public interface ActionModeMoveListener {
    void onActionModeMoveBooks(CustomRecyclerViewAdapter adapter, ActionMode mode);
}
