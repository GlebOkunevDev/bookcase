package bookcase.grimald.com.interfaces;

import android.support.v7.view.ActionMode;

import bookcase.grimald.com.adapter.CustomRecyclerViewAdapter;

/**
 * Created by user on 12.10.2016.
 */

public interface ActionModeDeleteListener {
    void onActionModeDeleteClick(CustomRecyclerViewAdapter adapter, ActionMode mode);
}
