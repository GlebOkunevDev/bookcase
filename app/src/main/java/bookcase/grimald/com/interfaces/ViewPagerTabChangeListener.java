package bookcase.grimald.com.interfaces;

/**
 * Created by user on 14.10.2016.
 */

public interface ViewPagerTabChangeListener {
    void onTabChangeSelect(int position);
    void onTabChangeUnselect(int position);
}
