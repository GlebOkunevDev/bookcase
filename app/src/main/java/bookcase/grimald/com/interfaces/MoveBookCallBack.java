package bookcase.grimald.com.interfaces;

/**
 * Interface  for sending request for updating data in the lists after books were moved from one list to another
 * Created by user on 01.11.2016.
 */

public interface MoveBookCallBack {
    void onMoveBook(int status);
}
