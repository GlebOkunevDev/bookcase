package bookcase.grimald.com.interfaces;

import bookcase.grimald.com.model.Book;

/**
 * Created by user on 16.09.2016.
 */
public interface FragmentRequestCallback {
    void onFragRequest(int request, Book book);
}
