package bookcase.grimald.com.interfaces;

import android.view.View;

/**
 * Created by user on 28.09.2016.
 */

public interface RecyclerViewClickListener {
    void onClick(View view, int position);
    void onLongClick(View view,int position);
}
