package bookcase.grimald.com.interfaces;

/**
 * Created by user on 06.10.2016.
 */

public interface OnBackPressedListener {
    void onBackPressed();
}
