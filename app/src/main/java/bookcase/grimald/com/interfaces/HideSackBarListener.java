package bookcase.grimald.com.interfaces;

/**
 * Interface for listen when SnackBar need hide in the BookListFragment
 * Created by user on 13.10.2016.
 */

public interface HideSackBarListener {
    void onHideSnackBar(int position);
}
