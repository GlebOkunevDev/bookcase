package bookcase.grimald.com.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by user on 27.09.2016.
 */

public class Book extends RealmObject implements Parcelable {

    @PrimaryKey
    private int mId;
    @Required
    private String mName;
    private String mAuthor;
    private String mGenre;
    private String mDescription;
    private String mRecommendation;
    private Date mStartReading;
    private Date mEndReading;
    private int mStatus;
    private Date mLastUpdate;
    private Date mBookCreateDate;
    private String mLittleImagePath;
    private String mBigImagePath;

    /**This array duplicate in the string.xml for using it on Activities*/
    private static final String[] mBookStatus = {"reading", "read", "recommended"};

    public Book() {
        mBookCreateDate = Calendar.getInstance().getTime();
    }

    public Book(String mName, String mAuthor, String mGenre, String mDescription, String mRecommendation) {
        this.mName = mName;
        this.mAuthor = mAuthor;
        this.mGenre = mGenre;
        this.mDescription = mDescription;
        this.mRecommendation = mRecommendation;
        mStatus = 2;
        mLastUpdate = Calendar.getInstance().getTime();
    }

    public Book(String mName, String mAuthor, String mGenre, String mDescription, Date mStartReading) {
        this.mName = mName;
        this.mAuthor = mAuthor;
        this.mGenre = mGenre;
        this.mDescription = mDescription;
        this.mStartReading = mStartReading;
        mStatus = 0;
        mLastUpdate = Calendar.getInstance().getTime();
    }

    public Book(String mName, String mAuthor, String mGenre, String mDescription, String mRecommendation, Date mStartReading) {
        this.mName = mName;
        this.mAuthor = mAuthor;
        this.mGenre = mGenre;
        this.mDescription = mDescription;
        this.mRecommendation = mRecommendation;
        this.mStartReading = mStartReading;
        mStatus = 0;
        mLastUpdate = Calendar.getInstance().getTime();
    }

    public Book(String mName, String mAuthor, String mGenre, String mDescription, Date mStartReading, Date mEndReading) {
        this.mName = mName;
        this.mAuthor = mAuthor;
        this.mGenre = mGenre;
        this.mDescription = mDescription;
        this.mStartReading = mStartReading;
        this.mEndReading = mEndReading;
        mStatus = 1;
        mLastUpdate = Calendar.getInstance().getTime();
    }

    public Book(String mName, String mAuthor, String mGenre, String mDescription, String mRecommendation, Date mStartReading, Date mEndReading) {
        this.mName = mName;
        this.mAuthor = mAuthor;
        this.mGenre = mGenre;
        this.mDescription = mDescription;
        this.mRecommendation = mRecommendation;
        this.mStartReading = mStartReading;
        this.mEndReading = mEndReading;
        mStatus = 1;
        mLastUpdate = Calendar.getInstance().getTime();
    }

    public String getName() {
        return mName;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public String getGenre() {
        return mGenre;
    }

    public int getId() {
        return mId;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getRecommendation() {
        return mRecommendation;
    }

    public Date getStartReading() {
        return mStartReading;
    }

    public Date getEndReading() {
        return mEndReading;
    }

    public Date getLastUpdate() {
        return mLastUpdate;
    }

    public int getStatus() {
        return mStatus;
    }

    public Date getBookCreateDate() {
        return mBookCreateDate;
    }

    public String getLittleImagePath() {
        return mLittleImagePath;
    }

    public String getBigImagePath() {
        return mBigImagePath;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public void setAuthor(String mAuthor) {
        this.mAuthor = mAuthor;
    }

    public void setGenre(String mGenre) {
        this.mGenre = mGenre;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public void setRecommendation(String mRecommendation) {
        this.mRecommendation = mRecommendation;
    }

    public void setStartReading(Date mStartReading) {
        this.mStartReading = mStartReading;
    }

    public void setEndReading(Date mEndReading) {
        this.mEndReading = mEndReading;
    }

    public void setLastUpdate(Date mLastUpdate) {
        this.mLastUpdate = mLastUpdate;
    }

    public void setStatus(int mStatus) {
        this.mStatus = mStatus;
    }

    public void setBookCreateDate(Date mBookCreateDate) {
        this.mBookCreateDate = mBookCreateDate;
    }

    public void setLittleImagePath(String mImagePath) {
        this.mLittleImagePath = mImagePath;
    }

    public void setBigImagePath(String mImagePath) {
        this.mBigImagePath = mImagePath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mId);
        dest.writeString(this.mName);
        dest.writeString(this.mAuthor);
        dest.writeString(this.mGenre);
        dest.writeString(this.mDescription);
        dest.writeString(this.mRecommendation);
        dest.writeSerializable(this.mStartReading);
        dest.writeSerializable(this.mEndReading);
        dest.writeInt(this.mStatus);
        dest.writeSerializable(this.mLastUpdate);
        dest.writeSerializable(this.mBookCreateDate);
        dest.writeString(this.mLittleImagePath);
        dest.writeString(this.mBigImagePath);
    }

    protected Book(Parcel in) {
        this.mId = in.readInt();
        this.mName = in.readString();
        this.mAuthor = in.readString();
        this.mGenre = in.readString();
        this.mDescription = in.readString();
        this.mRecommendation = in.readString();
        this.mStartReading = (Date) in.readSerializable();
        this.mEndReading = (Date) in.readSerializable();
        this.mStatus = in.readInt();
        this.mLastUpdate = (Date) in.readSerializable();
        this.mBookCreateDate = (Date) in.readSerializable();
        this.mLittleImagePath = in.readString();
        this.mBigImagePath = in.readString();
    }

    public static final Parcelable.Creator<Book> CREATOR = new Parcelable.Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel source) {
            return new Book(source);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (!mName.equals(book.mName)) return false;
        if (mAuthor != null ? !mAuthor.equals(book.mAuthor) : book.mAuthor != null) return false;
        return mGenre != null ? mGenre.equals(book.mGenre) : book.mGenre == null;
    }

    @Override
    public int hashCode() {
        int result = mId;
        result = 31 * result + mName.hashCode();
        result = 31 * result + (mAuthor != null ? mAuthor.hashCode() : 0);
        result = 31 * result + (mGenre != null ? mGenre.hashCode() : 0);
        return result;
    }
}
