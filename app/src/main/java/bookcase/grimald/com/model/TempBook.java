package bookcase.grimald.com.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Глеб on 22.12.2016.
 */

public class TempBook implements Parcelable{

    private int mId;
    private String mName;
    private String mAuthor;
    private String mGenre;
    private String mDescription;
    private String mRecommendation;
    private Date mStartReading;
    private Date mEndReading;
    private int mStatus;
    private Date mLastUpdate;
    private Date mBookCreateDate;
    private String mLittleImagePath;
    private String mBigImagePath;

    public TempBook() {
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public String getGenre() {
        return mGenre;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getRecommendation() {
        return mRecommendation;
    }

    public Date getStartReading() {
        return mStartReading;
    }

    public Date getEndReading() {
        return mEndReading;
    }

    public int getStatus() {
        return mStatus;
    }

    public Date getLastUpdate() {
        return mLastUpdate;
    }

    public Date getBookCreateDate() {
        return mBookCreateDate;
    }

    public String getLittleImagePath() {
        return mLittleImagePath;
    }

    public String getBigImagePath() {
        return mBigImagePath;
    }

    public void setId(int id) {
        mId = id;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setAuthor(String author) {
        mAuthor = author;
    }

    public void setGenre(String genre) {
        mGenre = genre;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public void setRecommendation(String recommendation) {
        mRecommendation = recommendation;
    }

    public void setStartReading(Date startReading) {
        mStartReading = startReading;
    }

    public void setEndReading(Date endReading) {
        mEndReading = endReading;
    }

    public void setStatus(int status) {
        mStatus = status;
    }

    public void setLastUpdate(Date lastUpdate) {
        mLastUpdate = lastUpdate;
    }

    public void setBookCreateDate(Date bookCreateDate) {
        mBookCreateDate = bookCreateDate;
    }

    public void setLittleImagePath(String littleImagePath) {
        mLittleImagePath = littleImagePath;
    }

    public void setBigImagePath(String bigImagePath) {
        mBigImagePath = bigImagePath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mId);
        dest.writeString(this.mName);
        dest.writeString(this.mAuthor);
        dest.writeString(this.mGenre);
        dest.writeString(this.mDescription);
        dest.writeString(this.mRecommendation);
        dest.writeLong(this.mStartReading != null ? this.mStartReading.getTime() : -1);
        dest.writeLong(this.mEndReading != null ? this.mEndReading.getTime() : -1);
        dest.writeInt(this.mStatus);
        dest.writeLong(this.mLastUpdate != null ? this.mLastUpdate.getTime() : -1);
        dest.writeLong(this.mBookCreateDate != null ? this.mBookCreateDate.getTime() : -1);
        dest.writeString(this.mLittleImagePath);
        dest.writeString(this.mBigImagePath);
    }

    protected TempBook(Parcel in) {
        this.mId = in.readInt();
        this.mName = in.readString();
        this.mAuthor = in.readString();
        this.mGenre = in.readString();
        this.mDescription = in.readString();
        this.mRecommendation = in.readString();
        long tmpMStartReading = in.readLong();
        this.mStartReading = tmpMStartReading == -1 ? null : new Date(tmpMStartReading);
        long tmpMEndReading = in.readLong();
        this.mEndReading = tmpMEndReading == -1 ? null : new Date(tmpMEndReading);
        this.mStatus = in.readInt();
        long tmpMLastUpdate = in.readLong();
        this.mLastUpdate = tmpMLastUpdate == -1 ? null : new Date(tmpMLastUpdate);
        long tmpMBookCreateDate = in.readLong();
        this.mBookCreateDate = tmpMBookCreateDate == -1 ? null : new Date(tmpMBookCreateDate);
        this.mLittleImagePath = in.readString();
        this.mBigImagePath = in.readString();
    }

    public static final Creator<TempBook> CREATOR = new Creator<TempBook>() {
        @Override
        public TempBook createFromParcel(Parcel source) {
            return new TempBook(source);
        }

        @Override
        public TempBook[] newArray(int size) {
            return new TempBook[size];
        }
    };
}
