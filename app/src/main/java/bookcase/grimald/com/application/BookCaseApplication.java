package bookcase.grimald.com.application;

import android.app.Application;
import android.os.Environment;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.BuildConfig;
import com.crashlytics.android.core.CrashlyticsCore;

import io.fabric.sdk.android.Fabric;
import java.io.File;

import bookcase.grimald.com.constant.Constant;
import bookcase.grimald.com.database.Migration;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by user on 27.09.2016.
 */

public class BookCaseApplication extends Application {

    private static BookCaseApplication instance;
    private String mImagePath;

    public static BookCaseApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        /*Fabric.with(this, new Crashlytics());*/

        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();
        // Initialize Fabric with the debug-disabled crashlytics.
        Fabric.with(this, crashlyticsKit);

        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("books.realm")
                .schemaVersion(2)
                .migration(new Migration())
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

        /*mImagePath = this.getFilesDir() + File.separator + "book_cover" + File.separator;*/
        mImagePath = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + File.separator + Constant.PICTURE_DIRECTORY + File.separator;
    }

    public String getImagePath() {
        return mImagePath;
    }
}
