package bookcase.grimald.com.database;

import android.util.Log;

import bookcase.grimald.com.model.Book;
import io.realm.Realm;
import io.realm.RealmAsyncTask;
import io.realm.RealmResults;

/**
 * Class for work with Realm Database
 * Created by user on 19.10.2016.
 */

public class DatabaseHelper {

    private static DatabaseHelper instance;
    private  Realm mRealm;

    private RealmResults<Book> mBookList;

    private DatabaseHelper(){
        mRealm = Realm.getDefaultInstance();
        mBookList = mRealm.where(Book.class).findAll();
    }

    public static DatabaseHelper getInstance(){
        if(instance == null){
            instance = new DatabaseHelper();
        }
        return instance;
    }

    /**
     * Method for deleting the book at the Database
     * @param book book object
     */
    public void deleteBookFromDB(final Book book){
        if(mRealm == null)   return;
        if(book == null)    return;
        if(mBookList == null)   return;
        if(!book.isValid())  return;
        if(mRealm.isClosed())   setRealm();
        Log.d(getClass().getSimpleName(), "deleteBookFromDB");
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
//                Log.d(getClass().getSimpleName(), String.valueOf(realm.where(Book.class).equalTo("mName", book.getName()).findFirst()));
                if(!book.isManaged()){
                    for (int i = 0; i < mBookList.size(); i++) {
                        if(!mBookList.get(i).isValid()) continue;
                        if(book.getId() == mBookList.get(i).getId()){
                            mBookList.get(i).deleteFromRealm();
                            break;
                        }
                    }
                }else{
                    book.deleteFromRealm();
                }
            }
        });
    }

    /**
     * Method for creating or updating the book at the Database
     * @param book book object
     */
    public void createOrUpdateBookFromDB(final Book book){
        if(mRealm == null)   return;
        if(book == null)    return;
        if(!book.isValid()) return;
        if(mRealm.isClosed())   setRealm();
        Log.d(getClass().getSimpleName(), "createOrUpdateBookFromDB");
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(book);
//                Log.d(getClass().getSimpleName(), String.valueOf(realm.where(Book.class).equalTo("mName", book.getName()).findFirst()));
            }
        });
    }

    public Realm getRealm() {
        return mRealm;
    }

    /**
     * Method for adding Realm configuration and receiving the Realm object for work with the app database.
     * And receiving the list of all books which store at the database.
     */
    public void setRealm() {
        mRealm = Realm.getDefaultInstance();
        mBookList = mRealm.where(Book.class).findAll();
    }

    public RealmResults<Book> getBookList() {
        return mBookList;
    }
}
