package bookcase.grimald.com.database;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

/**
 * Class for migration from one schema to another
 * Created by user on 21.11.2016.
 */

public class Migration implements RealmMigration {

    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();

        /** Adding a new field "mImagePath" */
        if (oldVersion == 0) {
            schema.get("Book")
                    .addField("mImagePath", String.class);
            oldVersion++;
        }

        /** Adding a new field "mBigImagePath" and rename field "mImagePath"*/
        if (oldVersion == 1){
            schema.get("Book")
                    .renameField("mImagePath", "mLittleImagePath")
                    .addField("mBigImagePath", String.class);
            oldVersion++;
        }
    }
}
