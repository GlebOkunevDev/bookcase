package bookcase.grimald.com.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import bookcase.grimald.com.R;

/**
 * Created by user on 07.10.2016.
 */

public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    private String[] mGenreArray;
    private Context mContext;

    public CustomSpinnerAdapter(String[] mGenreArray, Context mContext) {
        this.mGenreArray = mGenreArray;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mGenreArray.length;
    }

    @Override
    public Object getItem(int i) {
        return mGenreArray[i];
    }

    @Override
    public long getItemId(int i) {
        return (long) i;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(mContext);
        txt.setTextSize(14);
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setPadding(16,4,0,4);
        txt.setText(mGenreArray[position]);
        txt.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryText));
        return  txt;
    }

    public View getView(int i, View view, ViewGroup viewgroup) {
        TextView txt = new TextView(mContext);
        txt.setGravity(Gravity.CENTER);
        txt.setTextSize(14);
        txt.setText(mGenreArray[i]);
        txt.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryText));
        return  txt;
    }
}
