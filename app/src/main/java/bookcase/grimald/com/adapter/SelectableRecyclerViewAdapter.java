package bookcase.grimald.com.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;

/**
 * Adapter required for do selections, remove selections, etc. at the RecyclerView ActionMode
 * Created by user on 12.10.2016.
 */

public abstract class SelectableRecyclerViewAdapter <VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH>{

    private SparseBooleanArray mSelectedItemsIds;

    public SelectableRecyclerViewAdapter() {
        mSelectedItemsIds = new SparseBooleanArray();
    }

    /**
     * Toggle selection methods
     * @param position
     */
    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    /**
     * Remove selected selections
     */
    public void removeSelection() {
        mSelectedItemsIds.clear();
        notifyDataSetChanged();
    }

    /**
     * Put or delete selected position into SparseBooleanArray
     * @param position
     * @param value
     */
    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);

        notifyDataSetChanged();
    }

    /**
     * Get total selected count
     * @return
     */
    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    /**
     * Return all selected ids
     * @return
     */
    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    /**
     * Return is item from position selected
     * @param position
     * @return
     */
    public boolean getItems(int position) {
        return mSelectedItemsIds.get(position);
    }
}
