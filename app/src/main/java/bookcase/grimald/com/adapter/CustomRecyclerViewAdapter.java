package bookcase.grimald.com.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import bookcase.grimald.com.R;
import bookcase.grimald.com.application.BookCaseApplication;
import bookcase.grimald.com.model.Book;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by user on 14.10.2016.
 */

public class CustomRecyclerViewAdapter extends SelectableRecyclerViewAdapter<CustomRecyclerViewAdapter.MyViewHolder>  {

    private List<Book> mBookList;
    private Context mContext;

    public List<Book> getBookList() {
        return mBookList;
    }

    public CustomRecyclerViewAdapter(List<Book> mBookList, Context context) {
        if(mBookList == null)   return;
        this.mBookList = new ArrayList<>();
        this.mBookList.addAll(mBookList);
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_recyclerview_item, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.mTextName.setText(mBookList.get(position).getName());
        holder.mTextAuthor.setText(mBookList.get(position).getAuthor());
        holder.mTextGenre.setText(mBookList.get(position).getGenre());

        if(mBookList.get(position).getLittleImagePath() != null) {
            holder.mProgressBar.setVisibility(View.VISIBLE);
            Picasso.with(mContext).load(new File(BookCaseApplication.getInstance().getImagePath() + mBookList.get(position).getLittleImagePath())).fit().centerInside()
                    .error(R.drawable.book_cover_not_available).centerInside()
                    .into(holder.mBookCover, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.mProgressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            Log.e(getClass().getSimpleName(), "Picasso image load error at position: " + String.valueOf(holder.getAdapterPosition()));
                        }
                    });
        }else{
            Picasso.with(mContext).load(R.drawable.book_cover_not_available).fit().centerInside()
                    .error(R.drawable.book_cover_not_available).centerInside()
                    .into(holder.mBookCover);
        }

        /*holder.mBookCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowBookCoverDialogFragment dialogFragment = ShowBookCoverDialogFragment.newInstance(mBookList.get(position).getBigImagePath(), 0);
                dialogFragment.show(((Activity) mContext).getFragmentManager(), mContext.getResources().getString(R.string.tag_show_book_cover_dialog));
            }
        });*/

        /** Hide divider after last list item **/
        /*int visible = (position == getItemCount()-1) ? View.GONE : View.VISIBLE;
        holder.mDivider.setVisibility(visible);*/

        /** Change background color of the selected items in list view  **/
        /*holder.itemView
                .setBackgroundColor(getItems(position) ? 0x9934B5E4
                        : Color.TRANSPARENT);*/

        /** Change background color of the selected CardView in list view  **/
        holder.mCardView.setCardBackgroundColor(getItems(position) ? ContextCompat.getColor(mContext, R.color.colorSelectedCardview)
                        : ContextCompat.getColor(mContext, R.color.colorRecyclerViewBackground));

        /** Change margins for the first (marginTop) and the last (marginBottom) items in list view*/
        /*RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        if(getItemCount() == 1)
            holder.mCardView.setLayoutParams(setMargin(layoutParams, 8, 8, 8, 8));
        if(position == 0 && getItemCount() > 1)
            holder.mCardView.setLayoutParams(setMargin(layoutParams, 8, 8, 8, 1));
        if(position == getItemCount()-1 && getItemCount() > 1)
            holder.mCardView.setLayoutParams(setMargin(layoutParams, 8, 1, 8, 8));*/
    }

    @Override
    public int getItemCount() {
        return (mBookList != null ? mBookList.size() : 0);
    }

    static class MyViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        @BindView(R.id.iv_book_cover) ImageView mBookCover;
        @BindView(R.id.rv_item_tv_name) TextView mTextName;
        @BindView(R.id.rv_item_tv_author) TextView mTextAuthor;
        @BindView(R.id.rv_item_tv_genre) TextView mTextGenre;
        /*@BindView(R.id.rv_item_divider) View mDivider;*/
        @BindView(R.id.rv_item_progress_book_cover) ProgressBar mProgressBar;
        @BindView(R.id.layout_item_cardview) CardView mCardView;

        MyViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            mCardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }

    /**
     * Method for converting dp to px (http://www.pcsalt.com/android/set-margins-in-dp-programmatically-android/)
     * @param context Context
     * @param dimen dimension in dp
     * @return dimension in px
     */
    private static int getPixelValue(Context context, int dimen) {
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dimen,
                resources.getDisplayMetrics()
        );
    }

    /**
     * Method for setting new view margins
     * @param layoutParams RelativeLayout.LayoutParams
     * @param margin array of margin dimensions in dp
     * @return layoutParams with the necessary dimensions of margins
     */
    private RelativeLayout.LayoutParams setMargin(RelativeLayout.LayoutParams layoutParams, int ... margin){
        int left = getPixelValue(mContext, margin[0]);
        int top = getPixelValue(mContext, margin[1]);
        int right = getPixelValue(mContext, margin[2]);
        int bottom = getPixelValue(mContext, margin[3]);
        layoutParams.setMargins(left, top, right, bottom);
        return layoutParams;
    }

    /** Methods for updating data in the adapter */

    /**
     * Add a new list in the adapter
     * @param books list of the book objects
     */
    public void setListContent(List<Book> books){
        if(mBookList == null || books == null)   return;
        this.mBookList = books;
        notifyDataSetChanged();
        /*notifyItemRangeChanged(0, mBookList.size());*/
    }

    /**
     * Deleting item knowing it position
     * @param position the book position which must be deleted
     */
    public void removeAt(int position) {
        if(mBookList == null)   return;
        mBookList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(0, mBookList.size());
    }

    /**
     * Deleting item knowing it
     * @param book book object
     */
    public void removeAt(Book book) {
        if(mBookList == null || book == null)   return;
        for(int i = 0; i < mBookList.size(); i++) {
            if (book.getId() == mBookList.get(i).getId()) {
                mBookList.remove(i);
                /*notifyDataSetChanged();*/
                notifyItemRemoved(i);
                notifyItemRangeChanged(0, mBookList.size());
                break;
            }
        }
    }

    /**
     * Replacement item in the list if it was updated
     * @param book book object
     */
    public void updateAt(Book book){
        if(mBookList == null || book == null)   return;
        for(int i = 0; i < mBookList.size(); i++) {
            if (book.getId() == mBookList.get(i).getId()) {
                mBookList.remove(i);
                mBookList.add(i, book);
                notifyDataSetChanged();
                /*notifyItemChanged(i, book);
                notifyItemRangeChanged(i, 1);*/
                break;
            }
        }
    }

    /**
     * Add a new item in the list
     * @param book book object
     */
    public void createAt(Book book){
        if(mBookList == null || book == null)   return;
        if(mBookList.size() != 0 && mBookList.get(mBookList.size() - 1).getId() == book.getId()) return;
        mBookList.add(book);
        notifyDataSetChanged();
        /*notifyItemInserted(mBookList.size() - 1);
        notifyItemRangeChanged(0, mBookList.size());*/
    }

    /**
     * Add a new item in the list at the known position
     * @param book book object
     */
    public void createAt(Book book, int position){
        if(mBookList == null || book == null)   return;
        mBookList.add(position, book);
        notifyDataSetChanged();
        /*notifyItemInserted(position);
        notifyItemRangeChanged(0, mBookList.size());*/
    }
}
