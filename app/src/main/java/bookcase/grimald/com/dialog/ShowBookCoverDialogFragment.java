package bookcase.grimald.com.dialog;

import android.app.Dialog;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;

import bookcase.grimald.com.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by user on 24.11.2016.
 */

public class ShowBookCoverDialogFragment extends DialogFragment implements View.OnClickListener{

    public static final String IMAGE_PATH = "image_path";
    public static final String DIALOG_STATUS = "dialog_status";
    public static final int SAVE_RESULT = 1;
    public static final int DELETE_IMAGE = 2;
    private static final int REQUEST_SELECT_IMAGE = 3;

    private Unbinder unbinder;
    @BindView(R.id.iv_show_book_cover) ImageView mImage;
    @BindView(R.id.tv_show_cover_dialog_load)  TextView mLoadNewImage;
    @BindView(R.id.tv_show_cover_dialog_delete)  TextView mDelete;
    @BindView(R.id.progress_show_book_cover) ProgressBar mProgressBar;
    @BindView(R.id.layout_book_cover_button) LinearLayout mButtonLayout;


    private String mImagePath;
    private int mDialogStatus;

    public ShowBookCoverDialogFragment() {
    }

    public static ShowBookCoverDialogFragment newInstance(String imagePath, int status) {
        ShowBookCoverDialogFragment dialogFragment = new ShowBookCoverDialogFragment();
        Bundle args = new Bundle();
        args.putString(IMAGE_PATH, imagePath);
        args.putInt(DIALOG_STATUS, status);
        dialogFragment.setArguments(args);
        return dialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        if(getArguments() != null){
            mImagePath = getArguments().getString(IMAGE_PATH);
            mDialogStatus = getArguments().getInt(DIALOG_STATUS, 0);
        }
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_show_book_cover, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        unbinder = ButterKnife.bind(this, view);

        mProgressBar.setVisibility(View.VISIBLE);
        Picasso.with(getActivity()).load(new File(mImagePath)).fit().centerInside()
                .error(R.drawable.image_not_found).into(mImage, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                mProgressBar.setVisibility(View.GONE);
                mDelete.setClickable(true);
            }

            @Override
            public void onError() {
                Log.e("ShowBookCoverDialogFrag", "Picasso image load error at position");
                //If image not found than the delete button not clickable
                /*mDelete.setClickable(false);*/
            }
        });

        if(mDialogStatus == 0)
            mButtonLayout.setVisibility(View.GONE);

        mImage.setOnClickListener(this);
        mDelete.setOnClickListener(this);
        mLoadNewImage.setOnClickListener(this);
    }

    //TODO Разобраться с размерами картинки и самого диалога, если получится. Приоритет низкий.
    /*@Override
    public void onResume() {
        // Store access variables for window and blank point
        Window window = getDialog().getWindow();
        Point size = new Point();
        // Store dimensions of the screen in `size`
        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);

        *//*int height = WindowManager.LayoutParams.WRAP_CONTENT;
        int width = WindowManager.LayoutParams.WRAP_CONTENT;
        if(mImage.getHeight() <= (int) (size.y * 0.5)){
            height = mImage.getHeight();
            width = (int) (size.x * 0.5);
        }else if(mImage.getHeight() > (int) (size.y * 0.5) && mImage.getHeight() <= (int) (size.y * 0.85)) {
            height = mImage.getHeight();
            width = width/(mImage.getHeight()/height);
        }else if(mImage.getHeight() > (int) (size.y * 0.85)){
            height = (int) (size.y * 0.85);
            width = width/(mImage.getHeight()/height);
        }*//*

        window.setLayout((int) (size.x * 0.95), WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        // Call super onResume after sizing
        super.onResume();
    }*/

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(IMAGE_PATH, mImagePath);
        outState.putInt(DIALOG_STATUS, mDialogStatus);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null) {
            mImagePath = savedInstanceState.getString(IMAGE_PATH);
            mDialogStatus = savedInstanceState.getInt(DIALOG_STATUS, 0);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_show_book_cover:
                dismiss();
                break;
            case R.id.tv_show_cover_dialog_delete:
                getTarget(DELETE_IMAGE, null);
                dismiss();
                break;
            case R.id.tv_show_cover_dialog_load:
                Intent getImageFromGalleryIntent =
                        new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(getImageFromGalleryIntent, REQUEST_SELECT_IMAGE);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQUEST_SELECT_IMAGE:
                getTarget(SAVE_RESULT, data);
                break;
        }
        dismiss();
    }

    private void getTarget(int resultCode, Intent data){
        if(getTargetFragment() != null) {
            getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, data);
        }
    }
}
