package bookcase.grimald.com.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import bookcase.grimald.com.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by user on 30.09.2016.
 */
//FIXME открывать клавиатуру при открытие диалога (из-за изманения его размера в коде (onResume метод) это не происходит)
public class DescriptionDialogFragment extends DialogFragment implements View.OnClickListener{

    public static final String TITLE_TEXT = "title_text";
    public static final String HINT_TEXT = "hint_text";
    public static final String MAX_LENGTH = "max_length";
    public static final String DESCRIPTION_TEXT = "description_text";
    public static final String SAVE_NEW_DESCRIPTION = "save_new_description";
    public static final String DESCRIPTION_TEXT_LENGTH = "description_text_length";
    public static final int SAVE_RESULT = 1;
    public static final int EXCEEDED_RESULT = 2;

    @BindView(R.id.tv_dialog_change_data_title) TextView mDialogTitle;
    @BindView(R.id.et_book_description) EditText mDescription;
    @BindView(R.id.tv_description_dialog_cancel) TextView mDescriptionCancel;
    @BindView(R.id.tv_description_dialog_save) TextView mDescriptionSave;
    @BindView(R.id.text_input_layout_description) TextInputLayout mInputLayout;

    private String mDescriptionText, mTitleText, mHintText;
    private int mMaxLength;
    private boolean isMakeChanges;

    private Unbinder unbinder;

    public DescriptionDialogFragment() {
    }

    public static DescriptionDialogFragment newInstance(String description, String hint, String title, int count) {
        DescriptionDialogFragment dialogFragment = new DescriptionDialogFragment();
        Bundle args = new Bundle();
        args.putString(HINT_TEXT, hint);
        args.putString(TITLE_TEXT, title);
        args.putInt(MAX_LENGTH, count);
        args.putString(DESCRIPTION_TEXT, description);
        dialogFragment.setArguments(args);
        return dialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        if(getArguments() != null){
            mTitleText = getArguments().getString(TITLE_TEXT, "");
            mHintText = getArguments().getString(HINT_TEXT, "");
            mMaxLength = getArguments().getInt(MAX_LENGTH);
            mDescriptionText = getArguments().getString(DESCRIPTION_TEXT, "");
        }
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_description_change, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        unbinder = ButterKnife.bind(this, view);

        mInputLayout.setCounterMaxLength(mMaxLength);
        mInputLayout.setHint(mHintText);

        mDialogTitle.setText(mTitleText);
        mDescription.setText(mDescriptionText);

        if(mTitleText.equals(getResources().getString(R.string.book_author_dialog_title)))
            mDescription.setRawInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);

        mDescription.addTextChangedListener(new EditTextListener());

        mDescriptionCancel.setOnClickListener(this);
        mDescriptionSave.setOnClickListener(this);

        mDescription.requestFocus();
        /*getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);*/
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_description_dialog_cancel:
                isMakeChanges  = false;
                dismiss();
                break;
            case R.id.tv_description_dialog_save:
                if(getTargetFragment() != null) {
                    getTargetFragment().onActivityResult(getTargetRequestCode(), SAVE_RESULT, new Intent().putExtra(SAVE_NEW_DESCRIPTION, mDescriptionText));
                }
                isMakeChanges  = false;
                dismiss();
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TITLE_TEXT, mTitleText);
        outState.putString(HINT_TEXT, mHintText);
        outState.putString(DESCRIPTION_TEXT, mDescriptionText);
        outState.putInt(MAX_LENGTH, mMaxLength);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null) {
            mTitleText = savedInstanceState.getString(TITLE_TEXT);
            mHintText = savedInstanceState.getString(HINT_TEXT);
            mDescriptionText = savedInstanceState.getString(DESCRIPTION_TEXT);
            mMaxLength = savedInstanceState.getInt(MAX_LENGTH);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getTargetFragment() != null && isMakeChanges) {
            getTargetFragment().onActivityResult(getTargetRequestCode(), SAVE_RESULT, new Intent().putExtra(SAVE_NEW_DESCRIPTION, mDescriptionText));
        }
    }

    @Override
    public void onResume() {
        double screenCoef;
        //Choose the different coefficients for different devices
        int screenSize = getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
        if(screenSize == Configuration.SCREENLAYOUT_SIZE_LARGE || screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE)
            screenCoef = 0.6;
        else
            screenCoef = 0.85;

        // Store access variables for window and blank point
        Window window = getDialog().getWindow();
        Point size = new Point();
        // Store dimensions of the screen in `size`
        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);
        // Set the width of the dialog proportional to 85% of the screen width
        window.setLayout((int) (size.x * screenCoef), WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        // Call super onResume after sizing
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private class EditTextListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int count, int after) {
            isMakeChanges = true;
            mDescriptionText = s.toString().trim();
            if (s.length() >= mMaxLength + 5) {
                getTargetFragment().onActivityResult(getTargetRequestCode(), EXCEEDED_RESULT, new Intent()
                        .putExtra(SAVE_NEW_DESCRIPTION, mDescriptionText)
                        .putExtra(DESCRIPTION_TEXT_LENGTH, s.length()));
                isMakeChanges  = false;
                dismiss();
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }
}
