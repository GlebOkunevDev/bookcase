package bookcase.grimald.com.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Locale;

import bookcase.grimald.com.R;
import bookcase.grimald.com.application.BookCaseApplication;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by user on 31.10.2016.
 */

public class MoveBooksDialogFragment extends DialogFragment implements View.OnClickListener{
    public static final String MARKED_BOOKS = "marked_books";
    public static final String LIST_STATUS = "list_status";
    public static final String SAVE_NEW_STATUS = "save_new_status";
    public static final int SAVE_RESULT = 1;

    private Unbinder unbinder;

    @BindView(R.id.tv_dialog_move_title) TextView mTitle;
    @BindView(R.id.tv_move_dialog_cancel) TextView mCancel;
    @BindView(R.id.tv_move_dialog_save) TextView mMove;
    @BindView(R.id.radio_group_move_books) RadioGroup mRadioGroup;
    @BindView(R.id.rb_move_reading) RadioButton mRadioReading;
    @BindView(R.id.rb_move_read) RadioButton mRadioRead;
    @BindView(R.id.rb_move_will_read) RadioButton mRadioWillRead;

    private int mCountMarkedBooks;
    private int mListStatus;
    private int mNewBookStatus;

    public MoveBooksDialogFragment() {
    }

    public static MoveBooksDialogFragment newInstance(int count, int status) {
        MoveBooksDialogFragment dialogFragment = new MoveBooksDialogFragment();
        Bundle args = new Bundle();
        args.putInt(MARKED_BOOKS, count);
        args.putInt(LIST_STATUS, status);
        dialogFragment.setArguments(args);
        return dialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        if(getArguments() != null){
            mCountMarkedBooks = getArguments().getInt(MARKED_BOOKS, 1);
            mListStatus = getArguments().getInt(LIST_STATUS);
        }
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_move_books, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        unbinder = ButterKnife.bind(this, view);
        mNewBookStatus = -1;

        switch (mListStatus){
            case 0:
                mRadioReading.setEnabled(false);
                break;
            case 1:
                mRadioRead.setEnabled(false);
                break;
            case 2:
                mRadioWillRead.setEnabled(false);
                break;
        }

        mTitle.setText(setDialogTitle());

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch(i){
                    case R.id.rb_move_reading:
                        mNewBookStatus = 0;
                        break;
                    case R.id.rb_move_read:
                        mNewBookStatus = 1;
                        break;
                    case R.id.rb_move_will_read:
                        mNewBookStatus = 2;
                        break;
                }
            }
        });

        mCancel.setOnClickListener(this);
        mMove.setOnClickListener(this);
    }

    /**
     * The method for selecting a string resource, depending on the device localization: ru or other
     * @return String title
     */
    private String setDialogTitle(){
        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            locale = getResources().getConfiguration().getLocales().get(0);
        else
            locale = getResources().getConfiguration().locale;

        if(locale.getLanguage().equals("ru"))
            return getResources().getString(R.string.dialog_move_title_move_ru);
        else
            return String.format(getResources().getString(R.string.dialog_move_title_move),
                    getResources().getQuantityString(R.plurals.numberOfBooks, mCountMarkedBooks));
    }

    @Override
    public void onResume() {
        double screenCoef;
        //Choose the different coefficients for different devices
        int screenSize = getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
        if(screenSize == Configuration.SCREENLAYOUT_SIZE_LARGE || screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE)
            screenCoef = 0.6;
        else
            screenCoef = 0.85;

        // Store access variables for window and blank point
        Window window = getDialog().getWindow();
        Point size = new Point();
        // Store dimensions of the screen in `size`
        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);
        // Set the width of the dialog proportional to 85% of the screen width
        window.setLayout((int) (size.x * screenCoef), WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        // Call super onResume after sizing
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(MARKED_BOOKS, mCountMarkedBooks);
        outState.putInt(LIST_STATUS, mListStatus);
        outState.putInt(SAVE_NEW_STATUS, mNewBookStatus);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null) {
            mCountMarkedBooks = savedInstanceState.getInt(MARKED_BOOKS, 1);
            mListStatus = savedInstanceState.getInt(LIST_STATUS);
            mNewBookStatus = savedInstanceState.getInt(SAVE_NEW_STATUS);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_move_dialog_cancel:
                dismiss();
                break;
            case R.id.tv_move_dialog_save:
                if(getTargetFragment() != null) {
                    getTargetFragment().onActivityResult(getTargetRequestCode(), SAVE_RESULT, new Intent().putExtra(SAVE_NEW_STATUS, mNewBookStatus));
                }
                dismiss();
                break;
        }
    }
}