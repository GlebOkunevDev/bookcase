package bookcase.grimald.com.dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by user on 04.10.2016.
 */

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

    public static final String USER_DATE = "user_date";
    public static final String SAVE_START_END_READING_DATE = "save_start_end_reading_date";
    public static final int SAVE_RESULT_DATEPICKER = 1;

    private Calendar mUserDate;

    private int mDay, mMonth, mYear;

    public DatePickerFragment() {
    }

    public static DatePickerFragment newInstance(Date date) {
        DatePickerFragment fragment = new DatePickerFragment();
        Bundle args = new Bundle();
        args.putSerializable(USER_DATE, date);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        mUserDate = Calendar.getInstance();
        if(getArguments() != null){
            Date date = (Date) getArguments().getSerializable(USER_DATE);
            mUserDate.setTime(date);
        }/*else{
            mUserDate = Calendar.getInstance();
        }*/

        mYear = mUserDate.get(Calendar.YEAR);
        mMonth = mUserDate.get(Calendar.MONTH);
        mDay = mUserDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog pickerDialog = new DatePickerDialog(getActivity(), this, mYear, mMonth, mDay);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            DatePicker datePicker = pickerDialog.getDatePicker();
            datePicker.setFirstDayOfWeek(Calendar.MONDAY);
        }

        return pickerDialog;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        if(getTargetFragment() != null) {
            mUserDate.set(year, month, day);
            getTargetFragment().onActivityResult(getTargetRequestCode(), SAVE_RESULT_DATEPICKER, new Intent().putExtra(SAVE_START_END_READING_DATE, mUserDate));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(USER_DATE, mUserDate);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null)
            mUserDate = (Calendar) savedInstanceState.getSerializable(USER_DATE);
    }
}
