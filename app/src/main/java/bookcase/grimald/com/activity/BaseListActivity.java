package bookcase.grimald.com.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.design.widget.TabLayout;

import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import bookcase.grimald.com.R;
import bookcase.grimald.com.adapter.CustomPagerAdapter;
import bookcase.grimald.com.adapter.CustomRecyclerViewAdapter;
import bookcase.grimald.com.constant.Constant;
import bookcase.grimald.com.database.DatabaseHelper;
import bookcase.grimald.com.fragment.BookListFragment;
import bookcase.grimald.com.interfaces.HideSackBarListener;
import bookcase.grimald.com.interfaces.DeleteBookCallback;
import bookcase.grimald.com.interfaces.MoveBookCallBack;
import bookcase.grimald.com.interfaces.ViewPagerTabChangeListener;
import bookcase.grimald.com.model.Book;
import bookcase.grimald.com.service.ActionWithBookService;
import bookcase.grimald.com.util.BookListFragmentUtil;
import bookcase.grimald.com.util.BookListUtil;
import bookcase.grimald.com.util.ShowBookFragmentUtil;
import bookcase.grimald.com.util.TabChangeIconUtil;
import bookcase.grimald.com.util.TabLayoutUtil;
import butterknife.BindView;
import butterknife.ButterKnife;

import static bookcase.grimald.com.fragment.BookListFragment.mDeleteBookStatus;

public class BaseListActivity extends AppCompatActivity implements ViewPagerTabChangeListener, DeleteBookCallback, MoveBookCallBack {

    public static final String SAVE_SELECT_TAB = "save_select_tab";
    public static final String RECEIVER_INTENT_FILTER = "receiver_data_update_at_list";
    public static final String RECEIVER_REQUEST = "receiver_request";
    public static final String RECEIVER_BOOK = "receiver_book";

    @BindView(R.id.view_pager) ViewPager mViewPager;
    @BindView(R.id.tab_layout) TabLayout mTabLayout;
    @BindView(R.id.toolbar_list_activity) Toolbar mToolbar;
    @BindView(R.id.tv_empty_list_title) TextView mEmptyTitle;

    private CustomPagerAdapter mPagerAdapter;

    private int mSelectTab;
    private int[] mTabsIcons;
    private int[] mTabsSelectIcons;

    /*private HideSackBarListener mListener;*/
    private List<Book> mWorkList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_list);

        init();
    }

    public void init(){
        mTabsIcons = Constant.TAB_ICON;
        mTabsSelectIcons = Constant.TAB_SELECT_ICON;
        mPagerAdapter = TabLayoutUtil.setupViewPager(getSupportFragmentManager());

        ButterKnife.bind(this);

        if(mPagerAdapter == null)   return;
        if(mViewPager == null)  return;
        if(mTabLayout == null)  return;
        if(mToolbar == null)    return;

        setSupportActionBar(mToolbar);

        mViewPager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        for (int i = 0; i < mTabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = mTabLayout.getTabAt(i);
            if (tab != null)
                tab.setCustomView(TabLayoutUtil.getTabView(mTabsIcons[i], this));
        }

        TabLayoutUtil.selectTab(mSelectTab, this, mTabLayout, mTabsSelectIcons);

        mTabLayout.addOnTabSelectedListener(new TabChangeIconUtil(this, mTabsSelectIcons, mTabsIcons));

        LocalBroadcastManager.getInstance(this).registerReceiver(mUpdateDataReceiver,
                new IntentFilter(RECEIVER_INTENT_FILTER));

        /*onAttachInterface(mPagerAdapter.getItem(mViewPager.getCurrentItem()));*/

        setEmptyTitleText(0, -1);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVE_SELECT_TAB, mTabLayout.getSelectedTabPosition());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState != null)
            mSelectTab = savedInstanceState.getInt(SAVE_SELECT_TAB);
    }

    public void addBook(View view){
        if(mViewPager == null)  return;
        Intent bookIntent = new Intent(BaseListActivity.this, BaseShowBookActivity.class);
        bookIntent.putExtra(BaseShowBookActivity.STATUS, mViewPager.getCurrentItem());
        bookIntent.putExtra(BaseShowBookActivity.ID, -1);
        startActivity(bookIntent);

        finishActionMode(mViewPager.getCurrentItem());
    }

    private BroadcastReceiver mUpdateDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int request = intent.getIntExtra(RECEIVER_REQUEST, 0);
            setVisibilityEmptyTitleText(request);
        }
    };

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mUpdateDataReceiver);
        /*mListener = null;*/
        Log.d(getClass().getSimpleName(), "onDestroy");
        if(!BookListFragmentUtil.isMyServiceRunning(ActionWithBookService.class, this))
            BookListFragmentUtil.actionServiceStart(this, BookListUtil.getInstance().getBookUseStatus(mDeleteBookStatus), ActionWithBookService.DELETE_ACTION, -1);
        super.onDestroy();
    }

   /* public void onAttachInterface(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof HideSackBarListener) {
            mListener = (HideSackBarListener) fragment;
        }else{
            throw new IllegalStateException(
                    "Fragment must implement the HideSackBarListener.");
        }
    }*/

    @Override
    public void onTabChangeSelect(int position) {
        setEmptyTitleText(position, -1);
    }

    @Override
    public void onTabChangeUnselect(int position) {
        finishActionMode(position);
    }

    @Override
    public void onDeleteBookCallback(int status, int size) {
        setEmptyTitleText(status, size);
    }

    public void setEmptyTitleText(int listStatus, int listSize){
        if(listSize == -1) {
            mWorkList = BookListUtil.getInstance().getBookList(listStatus);
            if (mWorkList == null) return;
            listSize = mWorkList.size();
            Log.d(getClass().getSimpleName(), String.valueOf(listSize));
        }

        if(listSize == 0) {
            mEmptyTitle.setText(getResources().getStringArray(R.array.empty_titles)[listStatus]);
            mEmptyTitle.setVisibility(View.VISIBLE);
        }else{
            mEmptyTitle.setVisibility(View.GONE);
        }
    }

    public void setVisibilityEmptyTitleText(int request) {
        switch (request){
            case ShowBookFragmentUtil.REQUEST_TO_LIST_CREATE:
                if(mEmptyTitle.getVisibility() == View.VISIBLE)
                    mEmptyTitle.setVisibility(View.GONE);
                break;
            case ShowBookFragmentUtil.REQUEST_TO_LIST_DELETE:
                if(mViewPager == null)   break;
                mWorkList = BookListUtil.getInstance().getBookList(mViewPager.getCurrentItem());
                if(mWorkList == null) break;
                if(mEmptyTitle.getVisibility() == View.GONE && mWorkList.size() == 0) {
                    mEmptyTitle.setVisibility(View.VISIBLE);
                    mEmptyTitle.setText(getResources().getStringArray(R.array.empty_titles)[mViewPager.getCurrentItem()]);
                }
                break;
        }
    }

    private void hideSnackBar(int position){
        if(mPagerAdapter == null)   return;
        BookListFragment fragment = (BookListFragment) mPagerAdapter.getItem(position);
        if(fragment.getSnackBar() != null)
            fragment.getSnackBar().dismiss();
    }

    private void finishActionMode(int position) {
        if(mPagerAdapter == null)   return;
        BookListFragment fragment = (BookListFragment) mPagerAdapter.getItem(position);
        if(fragment.getActionMode() != null)
            fragment.getActionMode().finish();
    }

    @Override
    public void onMoveBook(int status) {
        if(mPagerAdapter == null)   return;
        BookListFragment fragment = (BookListFragment) mPagerAdapter.getItem(status);
        if(fragment.getAdapter() != null)
            fragment.getAdapter().setListContent(BookListUtil.getInstance().getBookList(status));
    }
}
