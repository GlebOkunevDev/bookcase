package bookcase.grimald.com.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import bookcase.grimald.com.R;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*setContentView(R.layout.activity_splash_screen);

        ActionBar myActionBar = getSupportActionBar();
        if(myActionBar != null)
            myActionBar.hide();

        nextScreen();*/

        startBaseActivity();
        finish();
    }

    private void nextScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startBaseActivity();
                finish();
            }
        }, 1000);
    }

    private void startBaseActivity() {
        startActivity(new Intent(SplashScreenActivity.this, BaseListActivity.class));
    }
}
