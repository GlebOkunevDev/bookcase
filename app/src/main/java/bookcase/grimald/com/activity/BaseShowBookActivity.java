package bookcase.grimald.com.activity;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import bookcase.grimald.com.R;
import bookcase.grimald.com.fragment.ShowBookFragment;
import bookcase.grimald.com.interfaces.FragmentRequestCallback;
import bookcase.grimald.com.interfaces.OnBackPressedListener;
import bookcase.grimald.com.model.Book;
import butterknife.BindView;
import butterknife.ButterKnife;

//TODO https://www.materialpalette.com/blue/deep-orange - App style
public class BaseShowBookActivity extends AppCompatActivity /*implements FragmentRequestCallback*/{
    private OnBackPressedListener mListener;

    @BindView(R.id.toolbar_show_book__activity) Toolbar mToolbar;

    public static final String STATUS = "status";
    public static final String ID = "id";
    private int mStatus;
    private int mId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_show_book);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        if(getIntent() == null) return;

        mStatus = getIntent().getIntExtra(STATUS, 0);
        mId = getIntent().getIntExtra(ID, -1);

        startFragment(new ShowBookFragment().newInstance(mId, mStatus), getSupportFragmentManager());
    }

    public void startFragment(Fragment fragment, FragmentManager fragmentManager){
        String tag = ((Object) fragment).getClass().getSimpleName();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment_container, fragment, tag);
//        ft.addToBackStack(tag);
        ft.commit();

        onAttachInterface(fragment);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putInt(STATUS, mStatus);
        outState.putInt(ID, mId);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState != null) {
            mStatus = savedInstanceState.getInt(STATUS, 0);
            mId = savedInstanceState.getInt(ID, -1);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if(mListener != null){
            mListener.onBackPressed();
        }
    }

    public void onAttachInterface(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof OnBackPressedListener) {
            mListener = (OnBackPressedListener) fragment;
        }else{
            throw new IllegalStateException(
                    "Fragment must implement the OnBackPressedListener.");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mListener = null;
    }

    /*@Override
    public void onFragRequest(int request, Book book) {
        Log.i(getClass().getSimpleName(), String.valueOf(request));
        Log.i(getClass().getSimpleName(), book.toString());
        Intent intent = new Intent(BaseListActivity.RECEIVER_INTENT_FILTER);
        intent.putExtra(BaseListActivity.RECEIVER_REQUEST, request);
        intent.putExtra(BaseListActivity.RECEIVER_BOOK, book);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }*/
}