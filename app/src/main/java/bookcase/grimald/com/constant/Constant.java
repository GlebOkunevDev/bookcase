package bookcase.grimald.com.constant;

import bookcase.grimald.com.R;

public class Constant {
    public static final int[] TAB_ICON = {R.drawable.ic_book, R.drawable.ic_book_shelf, R.drawable.ic_recomendation};
    public static final int[] TAB_SELECT_ICON = {R.drawable.ic_book_select, R.drawable.ic_book_shelf_select, R.drawable.ic_recomendation_select};

    public static final int BOOK_TITLE_LENGTH = 200;
    public static final int BOOK_AUTHOR_LENGTH = 100;
    public static final int BOOK_RECOMMENDATION_LENGTH = 100;
    public static final int DESCRIPTION_LENGTH = 500;

    public static final String PICTURE_DIRECTORY = "BookCover";
}
