package bookcase.grimald.com.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import bookcase.grimald.com.R;
import bookcase.grimald.com.activity.BaseShowBookActivity;
import bookcase.grimald.com.adapter.CustomRecyclerViewAdapter;
import bookcase.grimald.com.application.BookCaseApplication;
import bookcase.grimald.com.database.DatabaseHelper;
import bookcase.grimald.com.dialog.MoveBooksDialogFragment;
import bookcase.grimald.com.interfaces.ActionModeDeleteListener;
import bookcase.grimald.com.interfaces.ActionModeDestroyListener;
import bookcase.grimald.com.interfaces.ActionModeMoveListener;
import bookcase.grimald.com.interfaces.ActionModeShareListener;
import bookcase.grimald.com.interfaces.DeleteBookCallback;
import bookcase.grimald.com.interfaces.MoveBookCallBack;
import bookcase.grimald.com.interfaces.RecyclerViewClickListener;
import bookcase.grimald.com.model.Book;
import bookcase.grimald.com.util.BookListFragmentUtil;
import bookcase.grimald.com.util.BookListUtil;
import bookcase.grimald.com.util.RecyclerViewTouchListener;
import bookcase.grimald.com.util.ShareDataUtil;
import bookcase.grimald.com.util.ToolbarActionModeCallback;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;

public class BookListFragment extends Fragment implements ActionModeDeleteListener, ActionModeDestroyListener, ActionModeMoveListener, ActionModeShareListener {
    private String TAG = getClass().getSimpleName();

    public static final String BOOK_STATUS = "book_status";
    public static final String RECEIVER_INTENT_FILTER = "receiver_update_data_recyclerview_adapter";
    public static final String RECEIVER_REQUEST = "receiver_request";
    public static final String RECEIVER_BOOK = "receiver_book";
    private static final int REQUEST_MOVE_BOOKS = 3;
    public static final int mDeleteBookStatus = 3;

    @BindView(R.id.recycler_view_book_list) RecyclerView mRecyclerView;

    private Unbinder unbinder;
    private CustomRecyclerViewAdapter mAdapter;
    private ActionMode mActionMode;
    private Snackbar mSnackBar;

    private List<Book> mBookList;
    private SparseArray<Book> mSelectBookArray;
    private SparseBooleanArray mMoveBooks;
    private int mStartServiceType;
    private int mNewBookStatus;
    private int mStatus;

    private DeleteBookCallback mDeleteCallback;
    private MoveBookCallBack mMoveCallback;
    private SearchView mSearchView;

    public BookListFragment() {
    }

    public Snackbar getSnackBar() {
        return mSnackBar;
    }

    public ActionMode getActionMode() {
        return mActionMode;
    }

    public CustomRecyclerViewAdapter getAdapter() {
        return mAdapter;
    }

    public static BookListFragment newInstance(int status) {
        BookListFragment fragment = new BookListFragment();
        Bundle args = new Bundle();
        args.putInt(BOOK_STATUS, status);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        if (getArguments() != null) {
            mStatus = getArguments().getInt(BOOK_STATUS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_book_list, container, false);
        init(view);
        return view;
    }

    public void init(View view){
        mBookList = BookListUtil.getInstance().getBookList(mStatus);

        unbinder = ButterKnife.bind(this, view);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new CustomRecyclerViewAdapter(mBookList, getActivity());
        mRecyclerView.setAdapter(mAdapter);

        implementRecyclerViewClickListener();

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mUpdateDataReceiver,
                new IntentFilter(RECEIVER_INTENT_FILTER));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(BOOK_STATUS, mStatus);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null){
            mStatus = savedInstanceState.getInt(BOOK_STATUS);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        При использование ViewPager FATAL EXCEPTION: java.lang.NullPointerException
//        unbinder.unbind();
    }

    private void implementRecyclerViewClickListener(){
        if(mRecyclerView == null)   return;

        mRecyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(BookCaseApplication.getInstance(), mRecyclerView, new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, final int position) {

                    /*if (mActionMode != null) {
                        onListItemSelect(position);
                    }else if(view.getId() == R.id.iv_book_cover){
                            if(mAdapter == null)    return;
                            Log.d("111", "imageView.setOn");
                            ShowBookCoverDialogFragment dialogFragment = ShowBookCoverDialogFragment.newInstance(mAdapter.getBookList().get(position).getBigImagePath(), 0);
                            dialogFragment.show(getFragmentManager(), getResources().getString(R.string.tag_show_book_cover_dialog));
                    }else {
                            if (mAdapter == null) return;
                            if (mAdapter.getBookList() == null) return;
                            Log.d("111", "else {");
                            Intent bookIntent = new Intent(getActivity(), BaseShowBookActivity.class);
                            bookIntent.putExtra(BaseShowBookActivity.STATUS, mAdapter.getBookList().get(position).getStatus());
                            bookIntent.putExtra(BaseShowBookActivity.ID, mAdapter.getBookList().get(position).getId());
                            startActivity(bookIntent);
                    }*/

                if (mActionMode != null) {
                    onListItemSelect(position);
                }else{
                    if (mAdapter == null) return;
                    if (mAdapter.getBookList() == null) return;
                    Intent bookIntent = new Intent(getActivity(), BaseShowBookActivity.class);
                    bookIntent.putExtra(BaseShowBookActivity.STATUS, mAdapter.getBookList().get(position).getStatus());
                    bookIntent.putExtra(BaseShowBookActivity.ID, mAdapter.getBookList().get(position).getId());
                    startActivity(bookIntent);
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                if(mSnackBar != null && mSnackBar.isShown())    return;
                onListItemSelect(position);
            }
        }));
    }

    /**
     * The Local Broadcast Receiver for listening when book was created or deleted through ShowBookFragment
     */
    private BroadcastReceiver mUpdateDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int request = intent.getIntExtra(RECEIVER_REQUEST, 0);
            Book book = intent.getParcelableExtra(RECEIVER_BOOK);
            BookListFragmentUtil.onListDataChange(request, book, mAdapter, mBookList, mStatus);
            if(mAdapter == null)   return;
            if(mBookList == null)   return;
            mAdapter.setListContent(mBookList);
            if(request == BookListFragmentUtil.CREATE_BOOK) {
                if (mRecyclerView == null) return;
                mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
            }
        }
    };

    /**
     * Method for controlling ActionMode
     * @param position position of the selected item
     */
    private void onListItemSelect(int position) {
        if(mAdapter == null)    return;

        mAdapter.toggleSelection(position);//Toggle the selection

        boolean hasCheckedItems = mAdapter.getSelectedCount() > 0;//Check if any items are already selected or not

        if (hasCheckedItems && mActionMode == null)
            // there are some selected items, start the actionMode
            mActionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(new ToolbarActionModeCallback(mAdapter, this));
        else if (!hasCheckedItems && mActionMode != null)
            // there no selected items, finish the actionMode
            mActionMode.finish();

        if (mActionMode != null)
            //set action mode title on item selection
            mActionMode.setTitle(String.valueOf(mAdapter.getSelectedCount()));
    }

    /**
     * Method to handle the removal of the book through ActionMode.
     * If the user cancels the deletion then the marked books will be shown at previous positions.
     * Otherwise the marked books will be deleted.
     * @param bookArray array of the books which user marked for deletion
     */
    private void deleteBook(final SparseArray<Book> bookArray){
        if(bookArray == null)   return;
        if(mAdapter == null)    return;
        mStartServiceType = 1;

        for(int i = 0; i < bookArray.size(); i++) {
            final Book book = bookArray.get(bookArray.keyAt(i));
            DatabaseHelper.getInstance().getRealm().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    book.setStatus(mDeleteBookStatus);
                }
            });
            BookListUtil.getInstance().deleteBook(book, mStatus);
        }
        mDeleteCallback.onDeleteBookCallback(mStatus, mAdapter.getItemCount());

        /*createSnackBar(String.format(getResources().getString(R.string.snackbar_delete), String.valueOf(mAdapter.getSelectedCount()),
                getResources().getQuantityString(R.plurals.numberOfBooks, mAdapter.getSelectedCount())), bookArray);*/

        createSnackBar(String.format(getResources().getQuantityString(R.plurals.numberOfDeletedBooks, mAdapter.getSelectedCount()),
                        String.valueOf(mAdapter.getSelectedCount())), bookArray);
    }

    @Override
    public void onActionModeDeleteClick(CustomRecyclerViewAdapter adapter, ActionMode mode) {
        if(adapter == null) return;
        if(mode == null)    return;

        SparseBooleanArray selected = adapter.getSelectedIds();
        mActionMode = mode;

        if(mSelectBookArray == null)
            mSelectBookArray = new SparseArray<>();

        if(mSelectBookArray.size() != 0)
            mSelectBookArray.clear();

        for (int i = (selected.size() - 1); i >= 0; i--) {
            if (selected.valueAt(i)) {
                mSelectBookArray.put(selected.keyAt(i), adapter.getBookList().get(selected.keyAt(i)));
                adapter.removeAt(selected.keyAt(i));
            }
        }
        deleteBook(mSelectBookArray);
        mDeleteCallback.onDeleteBookCallback(mStatus, adapter.getItemCount());
        mActionMode.finish();
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        mActionMode = mode;

        if (mActionMode != null)
            mActionMode = null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DeleteBookCallback) {
            mDeleteCallback = (DeleteBookCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + "Activity must implement DeleteBookCallback");
        }
        if (context instanceof MoveBookCallBack) {
            mMoveCallback = (MoveBookCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + "Activity must implement MoveBookCallBack");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mDeleteCallback = null;
        mMoveCallback = null;
        Log.d(TAG, "onDetach() " + String.valueOf(mStatus));
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() " + String.valueOf(mStatus));
        //При отмене удаления или перемещения книги через ActionMode и последующего создания, созданная книга не отображалась в списке
        //Возможно, такая реакция из-за mMoveCallback.onMoveBook(mStatus) в методах restoreDeletedBook(bookArray) и restoreMovedBook(bookArray)
        /*if(mAdapter == null)   return;
        mAdapter.setListContent(mBookList);*/
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() " + String.valueOf(mStatus));
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy " + String.valueOf(mStatus));
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mUpdateDataReceiver);
        super.onDestroy();
    }

    @Override
    public void onActionModeMoveBooks(CustomRecyclerViewAdapter adapter, ActionMode mode) {
        if(adapter == null) return;
        if(mode == null)    return;

        mMoveBooks = adapter.getSelectedIds();
        mActionMode = mode;
        mAdapter = adapter;

        BookListFragmentUtil.openMoveDialog(REQUEST_MOVE_BOOKS, this, mAdapter.getSelectedCount(), mStatus);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_MOVE_BOOKS:
                mNewBookStatus =  data.getIntExtra(MoveBooksDialogFragment.SAVE_NEW_STATUS, -1);
                moveBookStepOne(mNewBookStatus);
                break;
        }
    }

    /**
     * Method for starting the book moving process to another list
     * @param newBookStatus the status of the list in which the book will be moved
     */
    private void moveBookStepOne(int newBookStatus){
        if(mAdapter == null)    return;
        if(mActionMode == null) return;
        if(mMoveBooks == null)  return;

        if(newBookStatus == -1)    return;

        if(mSelectBookArray == null)
            mSelectBookArray = new SparseArray<>();

        if(mSelectBookArray.size() != 0)
            mSelectBookArray.clear();

        for (int i = (mMoveBooks.size() - 1); i >= 0; i--) {
            if (mMoveBooks.valueAt(i)) {
                mSelectBookArray.put(mMoveBooks.keyAt(i), mAdapter.getBookList().get(mMoveBooks.keyAt(i)));
                mAdapter.removeAt(mMoveBooks.keyAt(i));
            }
        }

        moveBookStepTwo(mSelectBookArray, newBookStatus);
        mDeleteCallback.onDeleteBookCallback(mStatus, mAdapter.getItemCount());
        mActionMode.finish();
    }

    /**
     * Method for processing moving the book to another list through ActionMode.
     * If the user cancels moving then the marked books will be shown at previous list at their positions.
     * Otherwise the marked books will be moved.
     * @param bookArray array of the books which user marked for moving
     * @param newBookStatus the list status in which moves the book
     */
    private void moveBookStepTwo(final SparseArray<Book> bookArray, final int newBookStatus){
        if(bookArray == null)   return;
        if(mAdapter == null)    return;

        Resources res = getResources();
        mStartServiceType = 2;

        if(mStatus == 0 && mNewBookStatus == 2 && BookListUtil.getInstance().getBookList(mNewBookStatus).size() == 0)
            BookListUtil.getInstance().getBookList(mNewBookStatus);

        for(int i = 0; i < bookArray.size(); i++) {
            Log.d(TAG,  bookArray.get(bookArray.keyAt(i)).getName());
            final Book book = bookArray.get(bookArray.keyAt(i));
            DatabaseHelper.getInstance().getRealm().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    book.setStatus(newBookStatus);
                    book.setLastUpdate(Calendar.getInstance().getTime());
                    //Установка даты прочтения при переносе книги в список прочитанных из оставшихся
                    /*if(newBookStatus == 1) {
                        book.setEndReading(Calendar.getInstance().getTime());
                    }*/
                }
            });

            BookListUtil.getInstance().updateBook(book, mStatus);
        }
        mMoveCallback.onMoveBook(newBookStatus);
        mDeleteCallback.onDeleteBookCallback(mStatus, mAdapter.getItemCount());

        /*createSnackBar(String.format(res.getString(R.string.snackbar_move), String.valueOf(mAdapter.getSelectedCount()),
                res.getQuantityString(R.plurals.numberOfBooks, mAdapter.getSelectedCount()), res.getStringArray(R.array.list_titles)[newBookStatus]), bookArray);*/

        createSnackBar(String.format(res.getQuantityString(R.plurals.numberOfMovedBooks, mAdapter.getSelectedCount()),
                        String.valueOf(mAdapter.getSelectedCount()), res.getStringArray(R.array.list_titles)[newBookStatus]), bookArray);
    }

    /**
     * Method for restoring books in the previous list after moving if user canceled this process using Action at the SnackBar
     * @param bookArray the list of books which need to restoring
     */
    private void restoreMovedBook(final SparseArray<Book> bookArray){
        if(bookArray == null)   return;

        if(mNewBookStatus == -1)    return;

        for(int i = 0; i < bookArray.size(); i++) {
            final Book book = bookArray.get(bookArray.keyAt(i));
            DatabaseHelper.getInstance().getRealm().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    book.setStatus(mStatus);
                    book.setLastUpdate(Calendar.getInstance().getTime());
                }
            });
            BookListUtil.getInstance().updateBook(book, mNewBookStatus, bookArray.keyAt(i));
        }
        mMoveCallback.onMoveBook(mNewBookStatus);
        mMoveCallback.onMoveBook(mStatus);
    }

    /**
     * Method for restoring books in the previous list after deleting if user canceled this process using Action at the SnackBar
     * @param bookArray the list of books which need to restoring
     */
    private void restoreDeletedBook(final SparseArray<Book> bookArray){
        if(bookArray == null)   return;

        for(int i = 0; i < bookArray.size(); i++) {
            final Book book = bookArray.get(bookArray.keyAt(i));
            DatabaseHelper.getInstance().getRealm().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    book.setStatus(mStatus);
                }
            });
            BookListUtil.getInstance().createBook(book, bookArray.keyAt(i));
        }
        mMoveCallback.onMoveBook(mStatus);
    }

    /**
     * Method for creating and showing SnackBar with action if user deleted or moved the marked books through ActionMode.
     * @param message message for displaying at SnackBar
     * @param bookArray the list of books which need to restoring if user canceled this process using Action at the SnackBar
     */
    private void createSnackBar(String message, final SparseArray<Book> bookArray){
        if(mAdapter == null)    return;
        if(getView() == null)   return;

        mSnackBar = Snackbar.make(getView(), message, 1500)
                .setAction(R.string.snackbar_action_delete, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (mStartServiceType){
                            case 1:
                                restoreDeletedBook(bookArray);
                                break;
                            case 2:
                                restoreMovedBook(bookArray);
                                break;
                        }

                        mDeleteCallback.onDeleteBookCallback(mStatus, mAdapter.getItemCount());
                        BookListFragmentUtil.actionModeSelectArrayClean(mSelectBookArray);
                    }
                });
        mSnackBar.show();
    }

    @Override
    public void onActionModeShareBooks(CustomRecyclerViewAdapter adapter, ActionMode mode) {
        if(adapter == null) return;
        if(mode == null)    return;

        SparseBooleanArray selected = adapter.getSelectedIds();
        mActionMode = mode;
        mAdapter = adapter;

        ArrayList<Book> bookList = new ArrayList<>();
        for (int i = 0; i < selected.size(); i++) {
            if(selected.valueAt(i))
                bookList.add(mAdapter.getBookList().get(selected.keyAt(i)));
        }

        ShareDataUtil.shareData(getActivity(), mStatus, bookList);
        mActionMode.finish();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mSearchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(mBookList == null)   return false;
                if(mAdapter == null)   return false;
                if(mRecyclerView == null)   return false;

                final List<Book> filteredModelList = filter(mBookList, newText);
                mAdapter.setListContent(filteredModelList);
                mRecyclerView.scrollToPosition(0);
                return true;
            }
        });
    }

    /**
     * Method for filtering for a given parameter list
     * @param models a list of books that need to filter
     * @param query parameter for filtering the list of books
     * @return a filtered list of books
     */
    private static List<Book> filter(List<Book> models, String query) {
        final String lowerCaseQuery = query.toLowerCase();

        final List<Book> filteredModelList = new ArrayList<>();
        for (Book model : models) {
            final String text = model.getName().toLowerCase();
            if (text.contains(lowerCaseQuery)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }
}