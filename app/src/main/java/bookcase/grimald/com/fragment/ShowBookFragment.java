package bookcase.grimald.com.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Calendar;

import bookcase.grimald.com.R;
import bookcase.grimald.com.adapter.CustomSpinnerAdapter;
import bookcase.grimald.com.constant.Constant;
import bookcase.grimald.com.dialog.DatePickerFragment;
import bookcase.grimald.com.dialog.ShowBookCoverDialogFragment;
import bookcase.grimald.com.interfaces.OnBackPressedListener;
import bookcase.grimald.com.model.Book;
import bookcase.grimald.com.model.TempBook;
import bookcase.grimald.com.service.ImageWorkService;
import bookcase.grimald.com.util.BookListUtil;
import bookcase.grimald.com.util.ShowBookFragmentUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;

public class ShowBookFragment extends Fragment implements View.OnClickListener, OnBackPressedListener, AdapterView.OnItemSelectedListener{
    private String TAG = getClass().getSimpleName();

    private static final String BOOK_ID = "book_id";
    private static final String BOOK_STATUS = "book_status";
    private static final String BOOK_OBJECT_SAVE = "book_object_save";
    private static final String TEMP_BOOK_OBJECT_SAVE = "temp_book_object_save";
    private static final String BOOK_SAVE_FLAG = "book_save_flag";
    private static final String SAVE_IS_MAKE_CALLBACK = "save_is_make_callback";
    private static final String DELETE_IMAGE_PATH = "delete_image_path";
    private static final String DELETE_IMAGE_FLAG = "delete_image_flag";
    private static final int REQUEST_GET_BOOK_TITLE = 2;
    private static final int REQUEST_GET_BOOK_AUTHOR = 3;
    private static final int REQUEST_GET_RECOMMENDATION_TEXT = 4;
    private static final int REQUEST_GET_DESCRIPTION_TEXT = 1;
    private static final int REQUEST_GET_DATE_BOOK_START_READING = 5;
    private static final int REQUEST_GET_DATE_BOOK_END_READING = 6;
    private static final int REQUEST_SELECT_IMAGE = 7;
    private static final int REQUEST_PERMISSION = 100;
    private static final int REQUEST_SHOW_BOOK_COVER = 8;

    private int mId;
    private int mStatus;

    @BindView(R.id.tv_book_title) TextView mBookTitle;
    @BindView(R.id.tv_author) TextView mBookAuthor;
    @BindView(R.id.tv_book_recommendation) TextView mBookRecommendation;
    @BindView(R.id.tv_book_description) TextView mBookDescription;
    @BindView(R.id.spinner_genre) Spinner mBookGenre;
    @BindView(R.id.b_start_reading) Button mBookStartReading;
    @BindView(R.id.b_end_reading) Button mBookEndReading;
    @BindView(R.id.layout_recommendation) LinearLayout mLayoutRecommendation;
    @BindView(R.id.layout_buttons) LinearLayout mLayoutButtons;
    @BindView(R.id.fab_save_delete_book) FloatingActionButton mFab;
    @BindView(R.id.iv_description_book_cover) ImageView mBookCover;
    @BindView(R.id.progress_description_book_cover) ProgressBar mProgressBar;

    /*private FragmentRequestCallback mCallback;*/
    private Unbinder unbinder;
    private Realm mRealm;
    private Book mBook;
    private TempBook mTempBook;
    private boolean isDataChange;
    private boolean isMakeCallback;
    private String mLittleImagePath;
    private boolean isFileDelete;

    public ShowBookFragment() {
    }

    public static ShowBookFragment newInstance(int id, int status) {
        ShowBookFragment fragment = new ShowBookFragment();
        Bundle args = new Bundle();
        args.putInt(BOOK_ID, id);
        args.putInt(BOOK_STATUS, status);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mId = getArguments().getInt(BOOK_ID);
            mStatus = getArguments().getInt(BOOK_STATUS);
            Log.i(getClass().getSimpleName(), String.valueOf(mId));
            Log.i(getClass().getSimpleName(), String.valueOf(mStatus));
        }

        if (getActivity() instanceof AppCompatActivity) {
            ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

            if (actionBar != null) {
                setHasOptionsMenu(true);
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }

        if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_show_book, container, false);
        init(view);
        return view;
    }

    public void init(View view){
        unbinder = ButterKnife.bind(this, view);
        mRealm = Realm.getDefaultInstance();

        if(mStatus == 0){
            mLayoutRecommendation.setVisibility(View.GONE);
            mBookEndReading.setVisibility(View.GONE);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            param.setMargins(0,0,0,0);
            mBookStartReading.setLayoutParams(param);
        }else if(mStatus == 1){
            mLayoutRecommendation.setVisibility(View.GONE);
        }else{
            mLayoutButtons.setVisibility(View.GONE);
        }

        mBookDescription.setFilters(new InputFilter[] {new InputFilter.LengthFilter(Constant.DESCRIPTION_LENGTH)});

        mBookTitle.setOnClickListener(this);
        mBookAuthor.setOnClickListener(this);
        mBookRecommendation.setOnClickListener(this);
        mBookDescription.setOnClickListener(this);
        mBookStartReading.setOnClickListener(this);
        mBookEndReading.setOnClickListener(this);
        mFab.setOnClickListener(this);
        mBookCover.setOnClickListener(this);

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(getResources().getStringArray(R.array.book_genre), getActivity());
        mBookGenre.setAdapter(customSpinnerAdapter);
        mBookGenre.setOnItemSelectedListener(this);

        mTempBook = new TempBook();
        if(mId == -1){
            mBook = new Book();
            mBook.setStatus(mStatus);
            mBook.setId(BookListUtil.getInstance().createBookId());
            mTempBook = ShowBookFragmentUtil.copyBook(mBook, mTempBook);
            return;
        }

        mBook = BookListUtil.getInstance().getBookUseId(mId, mStatus);
        mTempBook = ShowBookFragmentUtil.copyBook(mBook, mTempBook);

        if(mBook == null)   return;
        isDataChange = ShowBookFragmentUtil.changeFabIcon(R.mipmap.ic_delete, false, mFab, getActivity());

        if(!TextUtils.isEmpty(mBook.getName())) mBookTitle.setText(mBook.getName());
        if(!TextUtils.isEmpty(mBook.getAuthor())) mBookAuthor.setText(mBook.getAuthor());
        if(!TextUtils.isEmpty(mBook.getRecommendation())) mBookRecommendation.setText(mBook.getRecommendation());
        if(!TextUtils.isEmpty(mBook.getDescription())) mBookDescription.setText(mBook.getDescription());

        if(!TextUtils.isEmpty(mBook.getGenre())){
            String[] genre = getResources().getStringArray(R.array.book_genre);
            for(int i = 0; i < genre.length; i++){
                if(genre[i].equals(mBook.getGenre())) {
                    mBookGenre.setSelection(i);
                    break;
                }
            }
        }

        if(mBook.getStartReading() != null) mBookStartReading.setText(DateFormat.format("dd.MM.yyyy", mBook.getStartReading()).toString());
        if(mBook.getEndReading() != null) mBookEndReading.setText(DateFormat.format("dd.MM.yyyy", mBook.getEndReading()).toString());

        if(mBook.getLittleImagePath() != null) {
            ShowBookFragmentUtil.loadImage(getActivity(), mProgressBar, mBookCover, mBook.getLittleImagePath());
        }
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(mTempBook == null)   return;
        /*if(mRealm == null)   mRealm = Realm.getDefaultInstance();*/

       /* mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {*/
                switch (requestCode) {
                    case REQUEST_GET_BOOK_TITLE:
                        isDataChange = ShowBookFragmentUtil.checkResultCode(resultCode, data, mBookTitle, Constant.BOOK_TITLE_LENGTH, getActivity(), mFab, getView());
                        mTempBook.setName(mBookTitle.getText().toString());
                        break;
                    case REQUEST_GET_BOOK_AUTHOR:
                        isDataChange = ShowBookFragmentUtil.checkResultCode(resultCode, data, mBookAuthor, Constant.BOOK_AUTHOR_LENGTH, getActivity(), mFab, getView());
                        mTempBook.setAuthor(mBookAuthor.getText().toString());
                        break;
                    case REQUEST_GET_RECOMMENDATION_TEXT:
                        isDataChange = ShowBookFragmentUtil.checkResultCode(resultCode, data, mBookRecommendation, Constant.BOOK_RECOMMENDATION_LENGTH, getActivity(), mFab, getView());
                        mTempBook.setRecommendation(mBookRecommendation.getText().toString());
                        break;
                    case REQUEST_GET_DESCRIPTION_TEXT:
                        isDataChange = ShowBookFragmentUtil.checkResultCode(resultCode, data, mBookDescription, Constant.DESCRIPTION_LENGTH, getActivity(), mFab, getView());
                        mTempBook.setDescription(mBookDescription.getText().toString());
                        break;
                    case REQUEST_GET_DATE_BOOK_START_READING:
                        isDataChange = ShowBookFragmentUtil.checkResultCode(resultCode, data, mBookStartReading, getActivity(), mFab);
                        mTempBook.setStartReading(((Calendar) data.getSerializableExtra(DatePickerFragment.SAVE_START_END_READING_DATE)).getTime());
                        break;
                    case REQUEST_GET_DATE_BOOK_END_READING:
                        isDataChange = ShowBookFragmentUtil.checkResultCode(resultCode, data, mBookEndReading, getActivity(), mFab);
                        mTempBook.setEndReading(((Calendar) data.getSerializableExtra(DatePickerFragment.SAVE_START_END_READING_DATE)).getTime());
                        break;
                    case REQUEST_SELECT_IMAGE:
                        loadImagePath(data);
                        isFileDelete = false;
                        break;
                    case REQUEST_SHOW_BOOK_COVER:
                        if(resultCode == ShowBookCoverDialogFragment.DELETE_IMAGE){
                            /*if(mTempBook == null)   break;*/
                            if(isFileDelete)
                                ShowBookFragmentUtil.startImageService(getActivity(), null, mLittleImagePath, null, ImageWorkService.DELETE_IMAGE);
//                                ImageUtil.deleteFile(mLittleImagePath);

                            mLittleImagePath =  mTempBook.getLittleImagePath();
                            final String bigImagePath =  mTempBook.getBigImagePath();
                            final boolean isDataChangeLocal = isDataChange;
                            isFileDelete = true;
                            mProgressBar.setVisibility(View.VISIBLE);
                            Picasso.with(getActivity()).load(R.drawable.book_cover_not_available).fit().centerInside().into(mBookCover, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    mProgressBar.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    Log.e(TAG, "Picasso image load error");
                                }
                            });
                            mTempBook.setBigImagePath(null);
                            mTempBook.setLittleImagePath(null);
                            if(!isDataChange)
                                isDataChange = ShowBookFragmentUtil.changeFabIcon(R.drawable.ic_save_fab, true, mFab, getActivity());
                            if(getView() == null)   break;
                            Snackbar snackBar = Snackbar.make(getView(), getResources().getString(R.string.dialog_show_snackbar_message), 1500)
                                    .setAction(R.string.dialog_show_snackbar_action, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            /*if(mRealm == null)   mRealm = Realm.getDefaultInstance();*/

                                            /*mRealm.executeTransaction(new Realm.Transaction() {
                                                                          @Override
                                                                          public void execute(Realm realm) {*/
                                            mTempBook.setBigImagePath(bigImagePath);
                                            mTempBook.setLittleImagePath(mLittleImagePath);
                                            isFileDelete = false;
                                                                          /*}
                                                                      });*/
                                            ShowBookFragmentUtil.loadImage(getActivity(), mProgressBar, mBookCover, mTempBook.getLittleImagePath());
                                            if(!isDataChangeLocal)
                                                isDataChange = ShowBookFragmentUtil.changeFabIcon(R.mipmap.ic_delete, false, mFab, getActivity());
                                        }
                                    });
                            snackBar.show();
                        }

                        if(resultCode == ShowBookCoverDialogFragment.SAVE_RESULT)
                            loadImagePath(data);

                        break;
                }
/*            }
        });*/
    }

    private void loadImagePath(Intent data){
        if(mTempBook == null)   return;
        String oldPath = mTempBook.getBigImagePath();
        mTempBook = ShowBookFragmentUtil.getImage(getActivity(), mTempBook, data, mProgressBar, mBookCover);
        if((oldPath == null && mTempBook.getBigImagePath() != null) || (oldPath != null && !oldPath.equals(mTempBook.getBigImagePath())))
            isDataChange = ShowBookFragmentUtil.changeFabIcon(R.drawable.ic_save_fab, true, mFab, getActivity());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(BOOK_ID, mId);
        outState.putInt(BOOK_STATUS, mStatus);
        outState.putParcelable(BOOK_OBJECT_SAVE, mBook);
        outState.putParcelable(TEMP_BOOK_OBJECT_SAVE, mTempBook);
        outState.putBoolean(BOOK_SAVE_FLAG, isDataChange);
        outState.putBoolean(SAVE_IS_MAKE_CALLBACK, isMakeCallback);
        outState.putString(DELETE_IMAGE_PATH, mLittleImagePath);
        outState.putBoolean(DELETE_IMAGE_FLAG, isFileDelete);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //TODO Think about the method of data recovery (mId, mStatus, mBook)(Fragment Lifecycle)
        if(savedInstanceState != null) {
            mId = savedInstanceState.getInt(BOOK_ID, -1);
            mStatus = savedInstanceState.getInt(BOOK_STATUS, 0);
            mBook = savedInstanceState.getParcelable(BOOK_OBJECT_SAVE);
            mTempBook = savedInstanceState.getParcelable(TEMP_BOOK_OBJECT_SAVE);
            isDataChange = savedInstanceState.getBoolean(BOOK_SAVE_FLAG);
            isMakeCallback = savedInstanceState.getBoolean(SAVE_IS_MAKE_CALLBACK);
            mLittleImagePath = savedInstanceState.getString(DELETE_IMAGE_PATH);
            isFileDelete = savedInstanceState.getBoolean(DELETE_IMAGE_FLAG);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //TODO Restore data
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mUpdateDataReceiver,
                new IntentFilter(ImageWorkService.GET_IMAGE_NAME_INTENT_FILTER));
    }

    @Override
    public void onPause() {
        super.onPause();
        if(isFileDelete)
            ShowBookFragmentUtil.startImageService(getActivity(), null, mLittleImagePath, null, ImageWorkService.DELETE_IMAGE);
//            ImageUtil.deleteFile(mLittleImagePath);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mUpdateDataReceiver);
        //TODO Save data
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mRealm != null && !mRealm.isClosed())
            mRealm.close();
    }

    @Override
    public void onClick(View view) {
        Resources res = getResources();
        switch (view.getId()){
            case R.id.tv_book_title:
                ShowBookFragmentUtil.openBookDescriptionDialog(REQUEST_GET_BOOK_TITLE, mBookTitle, res.getString(R.string.book_title_dialog_hint),
                        res.getString(R.string.book_title_dialog_title), Constant.BOOK_TITLE_LENGTH, this);
                break;
            case R.id.tv_author:
                ShowBookFragmentUtil.openBookDescriptionDialog(REQUEST_GET_BOOK_AUTHOR, mBookAuthor, res.getString(R.string.book_author_dialog_hint),
                        res.getString(R.string.book_author_dialog_title), Constant.BOOK_AUTHOR_LENGTH, this);
                break;
            case R.id.tv_book_recommendation:
                ShowBookFragmentUtil.openBookDescriptionDialog(REQUEST_GET_RECOMMENDATION_TEXT, mBookRecommendation, res.getString(R.string.book_recommendation_dialog_hint),
                        res.getString(R.string.book_recommendation_dialog_title), Constant.BOOK_RECOMMENDATION_LENGTH, this);
                break;
            case R.id.tv_book_description:
                ShowBookFragmentUtil.openBookDescriptionDialog(REQUEST_GET_DESCRIPTION_TEXT, mBookDescription, res.getString(R.string.description_dialog_hint),
                        res.getString(R.string.description_dialog_title), Constant.DESCRIPTION_LENGTH, this);
                break;
            case R.id.b_start_reading:
                ShowBookFragmentUtil.openDatePickerDialog(REQUEST_GET_DATE_BOOK_START_READING, mId, mTempBook.getStartReading(), this);
                break;
            case R.id.b_end_reading:
                ShowBookFragmentUtil.openDatePickerDialog(REQUEST_GET_DATE_BOOK_END_READING, mId, mTempBook.getEndReading(), this);
                break;
            case R.id.fab_save_delete_book:
                if(mTempBook == null)   return;

                if(!isDataChange && mTempBook.getLastUpdate() == null){
                    break;
                }

                if(isDataChange) {
                    mTempBook = ShowBookFragmentUtil.bookSaveMethod(mTempBook, mBook, mStatus, mId, getActivity(), getView());
                    isMakeCallback = true;
                    if(mTempBook.getLastUpdate() != null) {
                        isDataChange = ShowBookFragmentUtil.changeFabIcon(R.mipmap.ic_delete, false, mFab, getActivity());
                        ShowBookFragmentUtil.createSnackBar(getResources().getString(R.string.snackbar_save_book), getView());
                        mId = mTempBook.getId();
                    }
                }else{
                    ShowBookFragmentUtil.deleteBook(getActivity(), mBook, getActivity()/*, mCallback*/);
                }
                break;
            case R.id.iv_description_book_cover:

                int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (permissionCheck != PackageManager.PERMISSION_GRANTED)   break;

                //TODO вынести запуск диалога ??

                if(mTempBook == null)   return;
                if(mTempBook.getBigImagePath() != null){
                    ShowBookCoverDialogFragment dialogFragment = ShowBookCoverDialogFragment.newInstance(mTempBook.getBigImagePath(), 1);
                    dialogFragment.setTargetFragment(this, REQUEST_SHOW_BOOK_COVER);
                    dialogFragment.show(this.getFragmentManager(), getResources().getString(R.string.tag_show_book_cover_dialog));
                }else{
                    Intent getImageFromGalleryIntent =
                            new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    startActivityForResult(getImageFromGalleryIntent, REQUEST_SELECT_IMAGE);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (isDataChange) {
            ShowBookFragmentUtil.onBackPressedDialog(getActivity(), mTempBook, mBook, mStatus, mId, getActivity(), getView()/*, mCallback*/);
        }else{
            if(isMakeCallback) {
                /*ShowBookFragmentUtil.makeCallback(mBook, mId, mCallback);*/
                ShowBookFragmentUtil.sendRequest(ShowBookFragmentUtil.REQUEST_TO_LIST_CREATE, getActivity());
            }
            getActivity().finish();
        }
    }

    @Override
    public void onItemSelected(final AdapterView<?> adapterView, View view, final int i, long l) {
        Log.i(TAG, "onItemSelected " + String.valueOf(i));
        if(mTempBook == null)   return;
        if(mTempBook.getGenre() != null && mTempBook.getGenre().equals(adapterView.getItemAtPosition(i).toString()))    return;
        /*if(mRealm == null)   mRealm = Realm.getDefaultInstance();*/

        /*mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {*/
                if(i != 0){
                    mTempBook.setGenre(adapterView.getItemAtPosition(i).toString());
                    isDataChange = ShowBookFragmentUtil.changeFabIcon(R.drawable.ic_save_fab, true, mFab, getActivity());
                }else{
                    if(!TextUtils.isEmpty(mTempBook.getGenre())) {
                        mTempBook.setGenre("");
                        isDataChange = ShowBookFragmentUtil.changeFabIcon(R.drawable.ic_save_fab, true, mFab, getActivity());
                    }
                }
            /*}
        });*/
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        //spinner
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (isDataChange) {
                    ShowBookFragmentUtil.onNaveUtilsDialog(getActivity(), mTempBook, mBook, mStatus, mId, getActivity(), getView()/*, mCallback*/);
                }else{
                    if(isMakeCallback) {
                        /*ShowBookFragmentUtil.makeCallback(mBook, mId, mCallback);*/
                        ShowBookFragmentUtil.sendRequest(ShowBookFragmentUtil.REQUEST_TO_LIST_CREATE, getActivity());
                    }
                    NavUtils.navigateUpFromSameTask(getActivity());
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @TargetApi(23)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION:
                if (grantResults.length > 0
                        || grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent getImageFromGalleryIntent =
                            new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    startActivityForResult(getImageFromGalleryIntent, REQUEST_SELECT_IMAGE);

                } else {
                    ShowBookFragmentUtil.createSnackBar(getResources().getString(R.string.permission_write_external_storage), getView());
                }
                break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
            }
        }
    }

    /**
     * The Local Broadcast Receiver for listening when image (Book cover) compressed and copied to app path
     */
    private BroadcastReceiver mUpdateDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("mUpdateDataReceiver", "getReceiver");
            String imagePath = intent.getStringExtra(ImageWorkService.LITTLE_IMAGE_BOOK_COVER);
            mTempBook = ShowBookFragmentUtil.updateImagePath(getActivity(), mTempBook, mProgressBar, mBookCover, imagePath);
            isDataChange = ShowBookFragmentUtil.changeFabIcon(R.drawable.ic_save_fab, true, mFab, getActivity());
        }
    };

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentRequestCallback) {
            mCallback = (FragmentRequestCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentButtonClickCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }*/
}
