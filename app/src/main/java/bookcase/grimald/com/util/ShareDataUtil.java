package bookcase.grimald.com.util;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.text.format.DateFormat;

import java.util.Date;
import java.util.List;

import bookcase.grimald.com.R;
import bookcase.grimald.com.model.Book;

/**
 * Class for sharing data to another apps
 * Created by user on 15.11.2016.
 */

public class ShareDataUtil {

    /**
     * Method for creating StringBuilder object for sharing data to another apps
     * @param context Context
     * @param status status of the list
     * @param bookList list of the book objects
     * @return StringBuilder object
     */
    private static StringBuilder makeSendSubject(Context context, int status, List<Book> bookList){
        if(bookList == null)    return null;
        StringBuilder subject = new StringBuilder();
        Resources res = context.getResources();

        switch(status){
            case 0:
                subject.append(res.getString(R.string.share_data_title_reading)).append(System.getProperty("line.separator"));
                for (int i = 0; i < bookList.size(); i++) {
                    subject = createPartOfSubject(context, subject, bookList.get(i).getName(), bookList.get(i).getAuthor(),
                                                bookList.get(i).getGenre(), bookList.get(i).getDescription());
                    subject.append(res.getString(R.string.share_data_book_start_reading));
                    subject = isDateNotNull(subject, bookList.get(i).getStartReading());
                    subject.append(System.getProperty("line.separator"));
                }
                break;
            case 1:
                subject.append(res.getString(R.string.share_data_title_read)).append(System.getProperty("line.separator"));
                for (int i = 0; i < bookList.size(); i++) {
                    subject = createPartOfSubject(context, subject, bookList.get(i).getName(), bookList.get(i).getAuthor(),
                            bookList.get(i).getGenre(), bookList.get(i).getDescription());
                    subject.append(res.getString(R.string.share_data_book_start_reading));
                    subject = isDateNotNull(subject, bookList.get(i).getStartReading());
                    subject.append(res.getString(R.string.share_data_book_end_reading));
                    subject = isDateNotNull(subject, bookList.get(i).getEndReading());
                    subject.append(System.getProperty("line.separator"));
                }
                break;
            case 2:
                subject.append(res.getString(R.string.share_data_title_will_read)).append(System.getProperty("line.separator"));
                for (int i = 0; i < bookList.size(); i++) {
                    subject = createPartOfSubject(context, subject, bookList.get(i).getName(), bookList.get(i).getAuthor(),
                            bookList.get(i).getGenre(), bookList.get(i).getDescription());
                    subject.append(res.getString(R.string.share_data_book_recommendation));
                    subject = isDataNotNull(subject, bookList.get(i).getRecommendation());
                    subject.append(System.getProperty("line.separator"));
                }
                break;
        }

        return subject;
    }

    /**
     * Method for creating a common part of StringBuilder object
     * @param context Context
     * @param subject StringBuilder object to which need to add the data
     * @param name the name of the book
     * @param author the author of the book
     * @param genre the genre of the book
     * @param description the description of the
     * @return StringBuilder object
     */
    private static StringBuilder createPartOfSubject(Context context, StringBuilder subject, String name, String author, String genre, String description){
        if(subject == null) return null;
        Resources res = context.getResources();
        subject.append(res.getString(R.string.share_data_book_name))
                .append(name)
                .append(System.getProperty("line.separator"))
                .append(res.getString(R.string.share_data_book_author));
        subject = isDataNotNull(subject, author);
        subject.append(res.getString(R.string.share_data_book_genre));
        subject = isDataNotNull(subject, genre);
        subject.append(res.getString(R.string.share_data_book_description));
        subject = isDataNotNull(subject, description);
        return subject;
    }

    /**
     * Method for checking string. If string equals null to line added "-", string otherwise
     * @param subject StringBuilder object to which need to add a line
     * @param str the string which need to check
     * @return StringBuilder object
     */
    private static StringBuilder isDataNotNull(StringBuilder subject, String str){
        if(str != null)
            subject.append(str);
        else
            subject.append("-");
        subject.append(System.getProperty("line.separator"));
        return subject;
    }

    /**
     * Method for checking date. If date equals null to line added "-", date otherwise
     * @param subject StringBuilder object to which need to add a line
     * @param date the date which need to check
     * @return StringBuilder object
     */
    private static StringBuilder isDateNotNull(StringBuilder subject, Date date){
        if(date != null)
            subject.append(DateFormat.format("dd.MM.yyyy", date).toString());
        else
            subject.append("-");
        subject.append(System.getProperty("line.separator"));
        return subject;
    }

    /**
     * Method for sharing data to another apps
     * @param context Context
     * @param status status of the list
     * @param bookList list of the book objects
     */
    public static void shareData(Context context, int status, List<Book> bookList){
        StringBuilder sendSubject = makeSendSubject(context, status, bookList);
        if(sendSubject == null) return;

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, sendSubject.toString());
        sendIntent.setType("text/plain");
        if(sendIntent.resolveActivity(context.getPackageManager()) != null)
            context.startActivity(Intent.createChooser(sendIntent, context.getResources().getString(R.string.share_data_menu_title)));
    }
}
