package bookcase.grimald.com.util;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.util.Log;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.List;

import bookcase.grimald.com.R;
import bookcase.grimald.com.adapter.CustomRecyclerViewAdapter;
import bookcase.grimald.com.application.BookCaseApplication;
import bookcase.grimald.com.database.DatabaseHelper;
import bookcase.grimald.com.dialog.MoveBooksDialogFragment;
import bookcase.grimald.com.fragment.BookListFragment;
import bookcase.grimald.com.model.Book;
import bookcase.grimald.com.service.ActionWithBookService;

/**
 * Class-helper for the BookListFragment class
 * Created by user on 02.11.2016.
 */

public class BookListFragmentUtil {
    public static final int CREATE_BOOK = 1;
    public static final int DELETE_BOOK = 2;
    public static final int UPDATE_BOOK = 3;

    /**
     * Method for starting ActionWithBookService
     * @param context Context
     * @param mSelectBookArray array of the books objects which were sent to the service for deleting or moving in background
     * @param mStartServiceType action number that must perform service (1 - delete, 2 - move)
     * @param mNewBookStatus the list status in which moves the book
     */
    public static void actionServiceStart(Context context, ArrayList<Book> mSelectBookArray, int mStartServiceType, int mNewBookStatus){
        if(mSelectBookArray == null)    return;
        if(mStartServiceType == 2)  return;
        if(mSelectBookArray.size() > 0){
            /*ArrayList<Book> bookList = new ArrayList<>();
            for (int i = 0; i < mSelectBookArray.size(); i++) {
                if(mSelectBookArray.get(mSelectBookArray.keyAt(i)).isValid())
                    bookList.add(mSelectBookArray.get(mSelectBookArray.keyAt(i)));
            }*/
            Intent intent = new Intent(BookCaseApplication.getInstance(), ActionWithBookService.class);
//            intent.putParcelableArrayListExtra(ActionWithBookService.SELECT_BOOK_ARRAY, bookList);
            intent.putParcelableArrayListExtra(ActionWithBookService.SELECT_BOOK_ARRAY, mSelectBookArray);
            intent.putExtra(ActionWithBookService.TYPE_OF_ACTION_WITH_BOOK_ARRAY, mStartServiceType);
            intent.putExtra(ActionWithBookService.NEW_BOOK_STATUS, mNewBookStatus);
            context.startService(intent);

//            actionModeSelectArrayClean(mSelectBookArray);
        }else if(DatabaseHelper.getInstance().getRealm() != null && !DatabaseHelper.getInstance().getRealm().isClosed())
            DatabaseHelper.getInstance().getRealm().close();
    }

    /**
     * Method for checking is service work
     * @param serviceClass service which need to check
     * @param context Context
     * @return true if checking service is work, false otherwise
     */
    public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method for cleaning array which contains selected books through ActionMode
     * @param mSelectBookArray array of the select books objects
     */
    public static void actionModeSelectArrayClean(SparseArray<Book> mSelectBookArray){
        if(mSelectBookArray == null)   return;
        mSelectBookArray.clear();
    }

    /**
     * Method for opening MoveBooksDialog and selecting list in which user want to move the book
     * @param request request
     * @param fragment fragment from which opens a dialog (setTargetFragment)
     * @param count the number of select books
     * @param status the current list status
     */
    public static void openMoveDialog(int request, Fragment fragment, int count, int status){
        MoveBooksDialogFragment dialogFragment = MoveBooksDialogFragment.newInstance(count, status);
        dialogFragment.setTargetFragment(fragment, request);
        dialogFragment.show(fragment.getFragmentManager(), fragment.getResources().getString(R.string.tag_move_dialog));
    }

    /**
     * Method for updating RecyclerView adapter list when mUpdateDataReceiver get request
     * @param request request
     * @param book the book object which was created or deleted
     * @param mAdapter the RecyclerView adapter
     * @param mBookList the list which need to show in the RecyclerView
     * @param mStatus the current list status
     */
    public static void onListDataChange(int request, Book book, CustomRecyclerViewAdapter mAdapter, List<Book> mBookList, int mStatus) {
        if(book == null)    return;
        if(mAdapter == null)    return;
        if(mBookList == null)   return;
        if(book.getStatus() != mStatus)   return;
        switch (request){
            case CREATE_BOOK:
                Log.i("BookListFragmentUtil", "CREATE_BOOK");
                mAdapter.createAt(book);
                break;
            case DELETE_BOOK:
                Log.i("BookListFragmentUtil", "DELETE_BOOK");
                mAdapter.setListContent(mBookList);
                break;
            case UPDATE_BOOK:
                Log.i("BookListFragmentUtil", "UPDATE_BOOK");
                break;
        }
    }
}