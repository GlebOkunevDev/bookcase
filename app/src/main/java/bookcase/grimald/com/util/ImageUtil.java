package bookcase.grimald.com.util;

import android.content.Context;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import bookcase.grimald.com.R;
import bookcase.grimald.com.application.BookCaseApplication;
import bookcase.grimald.com.constant.Constant;
import bookcase.grimald.com.model.TempBook;

/**
 * Class for working with images
 * http://stackoverflow.com/questions/36467907/store-an-image-from-gallery-to-a-different-folder - see urls for more details
 * https://coderwall.com/p/wzinww/resize-an-image-in-android-to-a-given-width
 * Created by user on 22.11.2016.
 */

public class ImageUtil {

    private static String mLittleImageFileName;
    /*private String mCurrentPhotoPath;*/

    public static String getLittleImageFileName() {
        return mLittleImageFileName;
    }

    public static void copyFile(String path, Context context, TempBook book, boolean isRealPath) throws IOException {
        String realPath;
        if(isRealPath)
            realPath = path;
        else
            realPath = getRealPath(path, context);

        if(realPath == null) return;
        File sourceImageFile = new File(realPath);

        if (!sourceImageFile.exists())  return;
        if(book == null)    return;

        /*File root = new File(context.getFilesDir(), "book_cover");
        if(!root.exists())
            root.mkdirs();*/

        File root = getBookCoversStorageDir(context, Constant.PICTURE_DIRECTORY);
        if(root == null) {
            Toast.makeText(context, context.getResources().getString(R.string.toast_external_storage_exp), Toast.LENGTH_LONG).show();
            return;
        }

        String time = DateFormat.format("ddMMyyyy", book.getBookCreateDate()).toString();
        mLittleImageFileName = book.getId() + "_" + time + ".jpg";

        File destinationImageFile = new File(root, mLittleImageFileName);

        ByteArrayOutputStream outStream = compressImage(sourceImageFile);
        if(outStream.size() == 0){
            FileChannel source = new FileInputStream(sourceImageFile).getChannel();
            FileChannel destination = new FileOutputStream(destinationImageFile).getChannel();
            if (destination != null && source != null) {
                destination.transferFrom(source, 0, source.size());
            }
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }else{
            FileOutputStream outputStream = new FileOutputStream(destinationImageFile);
            outputStream.write(outStream.toByteArray());
            outputStream.flush();
            outputStream.close();
        }
    }

    /* Checks if external storage is available for read and write */
    private static boolean isExternalStorageWritable() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }

    private static File getBookCoversStorageDir(Context context, String dirName) {
        if(!isExternalStorageWritable()) {
            Log.e("ImageUtil.class", BookCaseApplication.getInstance().getResources().getString(R.string.toast_external_storage_exp));
            return null;
        }

        // Get the directory for the app's private pictures directory.
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), dirName);
        if(!file.exists())
            file.mkdirs();

        return file;
    }

    public static void deleteFile(String fileName){
        File file = new File(BookCaseApplication.getInstance().getImagePath() + fileName);
        file.delete();
    }

    private static ByteArrayOutputStream compressImage(File sourceImageFile){
        Bitmap b = BitmapFactory.decodeFile(sourceImageFile.getAbsolutePath());
        // original measurements
        int origWidth = b.getWidth();
        int origHeight = b.getHeight();

        final int destWidth = 200;//or the width you need

        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        if(origWidth > destWidth){
            // picture is wider than we want it, we calculate its target height
            int destHeight = origHeight/( origWidth / destWidth ) ;
            // we create an scaled bitmap so it reduces the image, not just trim it
            Bitmap b2 = Bitmap.createScaledBitmap(b, destWidth, destHeight, false);
            // compress to the format you want, JPEG, PNG...
            // 70 is the 0-100 quality percentage
            b2.compress(Bitmap.CompressFormat.JPEG,70 , outStream);
        }
        return outStream;
    }

    public static String getRealPath(String path, Context context){
        Uri uri = Uri.parse(path);
        String[] projection = { MediaStore.Images.Media.DATA };
//        Cursor cursor = new CursorLoader(context, uri, projection, null, null, null).loadInBackground();
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if(cursor == null)  return null;
        cursor.moveToFirst();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        String realPath = cursor.getString(column_index);
        cursor.close();
        return realPath;
    }

    /*private File createImageFile(Book book, Context context) throws IOException {
        if(book == null)    return null;
        String time = DateFormat.format("ddMMyyyy", book.getBookCreateDate()).toString();
        String name = book.getName().substring(0, 10);
        String mLittleImageFileName = book.getId() + time + "_" + name + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                mLittleImageFileName,   //prefix
                ".jpg",          //suffix
                storageDir       //directory
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }*/
}
