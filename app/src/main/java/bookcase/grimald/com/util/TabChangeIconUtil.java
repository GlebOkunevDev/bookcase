package bookcase.grimald.com.util;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import bookcase.grimald.com.R;
import bookcase.grimald.com.activity.BaseListActivity;
import bookcase.grimald.com.interfaces.ViewPagerTabChangeListener;

/**
 * Class for changing icon when tab selected/not selected
 * Created by user on 19.09.2016.
 */
public class TabChangeIconUtil  implements TabLayout.OnTabSelectedListener{
    private String TAG = getClass().getSimpleName();

    private Context mContext;
    private int[] mTabsSelectIcons;
    private int[] mTabsIcons;
    private ViewPagerTabChangeListener mListener;

    public TabChangeIconUtil(Context mContext, int[] mTabsSelectIcons, int[] mTabsIcons) {
        this.mContext = mContext;
        this.mTabsSelectIcons = mTabsSelectIcons;
        this.mTabsIcons = mTabsIcons;
        onAttachInterface((BaseListActivity) mContext);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        Log.i(TAG, "onTabSelected");
        if (tab.getCustomView().findViewById(R.id.icon) == null)   return;
        tab.getCustomView().findViewById(R.id.icon).setBackground(ContextCompat.getDrawable(mContext, mTabsSelectIcons[tab.getPosition()]));
        mListener.onTabChangeSelect(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        Log.i(TAG, "onTabUnselected");
        if (tab.getCustomView().findViewById(R.id.icon) == null)   return;
        tab.getCustomView().findViewById(R.id.icon).setBackground(ContextCompat.getDrawable(mContext, mTabsIcons[tab.getPosition()]));
        mListener.onTabChangeUnselect(tab.getPosition());
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        Log.i(TAG, "onTabReselected");
    }

    private void onAttachInterface(Activity activity) {
        if (activity instanceof ViewPagerTabChangeListener) {
            mListener = (ViewPagerTabChangeListener) activity;
        } else {
            throw new IllegalStateException(
                    "Activity must implement the ViewPagerTabChangeListener.");
        }
    }

}
