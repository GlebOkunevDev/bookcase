package bookcase.grimald.com.util;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import bookcase.grimald.com.application.BookCaseApplication;
import bookcase.grimald.com.database.DatabaseHelper;
import bookcase.grimald.com.fragment.BookListFragment;
import bookcase.grimald.com.model.Book;

/**
 * Class for work with data
 * Created by user on 28.09.2016.
 */

public class BookListUtil {

    private static List<Book> mBookList;
    private static List<Book> mBookReadList;
    private static List<Book> mBookEndList;
    private static List<Book> mBookRecommendList;

    private static BookListUtil instance;

    private BookListUtil(){
    }

    public static BookListUtil getInstance(){
        if(instance == null){
            instance = new BookListUtil();

            mBookList = DatabaseHelper.getInstance().getBookList();

            mBookReadList = new ArrayList<>();
            mBookEndList = new ArrayList<>();
            mBookRecommendList = new ArrayList<>();
        }
        return instance;
    }

    /**
     * Method for finding the book object in the BookList
     * @param id id of the book object
     * @param status status of the book
     * @return book object
     */
    public Book getBookUseId(int id, int status){
        List<Book> workList = getNecessaryList(status);
        if(workList == null)   return null;

        for (int i = 0; i < workList.size(); i++) {
            if(workList.get(i).getId() == id){
                return workList.get(i);
            }
        }

        return null;
    }

    /**
     * The method of finding all the books objects in the specified status BookList
     * @param status status of the books which need to find
     * @return list of the book objects
     */
    public ArrayList<Book> getBookUseStatus(int status){
        mBookList = DatabaseHelper.getInstance().getBookList();
        if(mBookList == null)   return null;
        if(DatabaseHelper.getInstance().getRealm().isClosed())  return null;
        ArrayList<Book> workList = new ArrayList<>();

        for (int i = 0; i < mBookList.size(); i++) {
            if(mBookList.get(i).getStatus() == status){
                workList.add(mBookList.get(i));
            }
        }
        return workList;
    }

    /**
     * Method for choices a book using status and adding it into the list
     * @param status status of the book ("reading - 0", "read - 1", "recommended - 2")
     * @return list of the book objects
     */
    public List<Book> getBookList(int status){
        Log.d(getClass().getSimpleName(), "getBookList");
        switch(status){
            case 0:
                loopForCreatingList(mBookReadList, status);
                return mBookReadList;
            case 1:
                loopForCreatingList(mBookEndList, status);
                return mBookEndList;
            case 2:
                loopForCreatingList(mBookRecommendList, status);
                return mBookRecommendList;
            default:
                Log.d(getClass().getSimpleName(), "return null;");
                return null;
        }
    }

    /**
     * Method for adding a new book object into the list
     * @param book book object
     */
    public void createBook(Book book){
        if(book == null)    return;

        switch (book.getStatus()){
            case 0:
                if(mBookReadList == null)   break;
                mBookReadList.add(book);
                break;
            case 1:
                if(mBookEndList == null)    break;
                mBookEndList.add(book);
                break;
            case 2:
                if(mBookRecommendList == null)    break;
                mBookRecommendList.add(book);
                break;
        }

        DatabaseHelper.getInstance().createOrUpdateBookFromDB(book);
        sendRequest(BookListFragmentUtil.CREATE_BOOK, book);
    }

    /**
     * Method for adding book object into the list of deleting process through ActionMode was canceled
     * @param book book object
     * @param position the position, in which the book was before deleting
     */
    public void createBook(Book book, int position){
        if(book == null)    return;

        switch (book.getStatus()){
            case 0:
                if(mBookReadList == null)   break;
                mBookReadList.add(position, book);
                break;
            case 1:
                if(mBookEndList == null)    break;
                mBookEndList.add(position, book);
                break;
            case 2:
                if(mBookRecommendList == null)    break;
                mBookRecommendList.add(position, book);
                break;
        }
        DatabaseHelper.getInstance().createOrUpdateBookFromDB(book);
    }

    /**
     * Method for deleting the book object from the list
     * @param book book object
     */
    public void deleteBook(Book book){
        if(book == null)  return;

        loopForDelete(getNecessaryList(book.getStatus()), book);
        DatabaseHelper.getInstance().deleteBookFromDB(book);
    }

    /**
     * Method for deleting (changing book status to 3) the book object from the list through ActionMode
     * @param book book object
     * @param status the current list status
     */
    public void deleteBook(Book book, int status){
        if(book == null)  return;

        loopForDelete(getNecessaryList(status), book);
        DatabaseHelper.getInstance().createOrUpdateBookFromDB(book);
    }

    /**
     * Method for updating the book object in the list
     * @param book book object
     */
    public void updateBook(Book book){
        if(book == null)  return;

        loopForUpdate(getNecessaryList(book.getStatus()), book);
        DatabaseHelper.getInstance().createOrUpdateBookFromDB(book);
        sendRequest(BookListFragmentUtil.UPDATE_BOOK, book);
    }

    /**
     * Method for updating the book object in the list when it moving from one list to another
     * @param book book object
     * @param oldStatus the old book status
     */
    public void updateBook(Book book, int oldStatus){
        if(book == null)  return;

        loopForUpdate(getNecessaryList(oldStatus), getNecessaryList(book.getStatus()), book);
        DatabaseHelper.getInstance().createOrUpdateBookFromDB(book);
    }

    /**
     * Method for updating the book object in the list when it moving from one list to another and user canceled this process
     * @param book book object
     * @param oldStatus the old book status
     * @param position the position, in which the book was before moving
     */
    public void updateBook(Book book, int oldStatus, int position){
        if(book == null)  return;

        loopForUpdate(getNecessaryList(oldStatus), getNecessaryList(book.getStatus()), book, position);
        DatabaseHelper.getInstance().createOrUpdateBookFromDB(book);
    }

    /**
     * Method for getting list of book object using status
     * @param status status of the books which stored in the list("reading - 0", "read - 1", "recommended - 2")
     * @return list of the book object
     */
    private List<Book> getNecessaryList(int status){
        switch (status){
            case 0:
                return mBookReadList;
            case 1:
                return mBookEndList;
            case 2:
                return mBookRecommendList;
            default:
                return null;
        }
    }

    /**
     * The loop for adding the book object in the list
     * @param list list of the book objects
     * @param status status of the book ("reading - 0", "read - 1", "recommended - 2")
     */
    private void loopForCreatingList(List<Book> list, int status) {
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance();
        if (list == null) return;
        if (databaseHelper.getRealm() == null) return;
        if (databaseHelper.getRealm().isClosed()) {
            databaseHelper.setRealm();
            mBookList = databaseHelper.getBookList();
            mBookReadList.clear();
            mBookEndList.clear();
            mBookRecommendList.clear();
        }

        if(list.size() == 0){
            Log.d(getClass().getSimpleName(), "list == null, " + String.valueOf(status));
            for (int i = 0; i < mBookList.size(); i++) {
                if (mBookList.get(i).getStatus() == status) {
                    list.add(mBookList.get(i));
                }
            }
        }
    }

    /**
     * The loop for deleting the book object in the list
     * @param bookList list of the book objects
     * @param book book object
     */
    private void loopForDelete(List<Book> bookList, Book book){
        if(bookList == null)   return;
        for (int i = 0; i < bookList.size(); i++) {
            if(bookList.get(i).getId() == book.getId()){
                bookList.remove(i);
                break;
            }
        }
    }

    /**
     * The loop for updating the book object in the list
     * @param bookList list of the book objects
     * @param book book object
     */
    private void loopForUpdate(List<Book> bookList, Book book){
        if(bookList == null)   return;
        for (int i = 0; i < bookList.size(); i++) {
            if(bookList.get(i).getId() == book.getId()){
                bookList.remove(i);
                bookList.add(i, book);
                break;
            }
        }
    }

    /**
     * The loop for updating the book object in the list if user moved it from one list to another
     * @param oldBookList the list of the book objects where the book was before moving
     * @param newBookList the list of the book objects where the book will be moved
     * @param book book object
     */
    private void loopForUpdate(List<Book> oldBookList, List<Book> newBookList, Book book){
        if(oldBookList == null)   return;
        if(newBookList == null)   return;
        oldBookList.remove(book);
        newBookList.add(book);
    }

    /**
     * The loop for updating the book object in the list if user canceled the move book process
     * @param oldBookList the list of the book objects where the book was before moving
     * @param newBookList the list of the book objects where the book will be moved
     * @param book book object
     * @param position the position to which need to add the book
     */
    private void loopForUpdate(List<Book> oldBookList, List<Book> newBookList, Book book, int position){
        if(oldBookList == null)   return;
        if(newBookList == null)   return;
        oldBookList.remove(book);
        newBookList.add(position, book);
    }

    /**
     * Generate ID for new books
     * @return id for a new book object
     */
    public int createBookId(){
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance();
        if(databaseHelper.getRealm() == null)   return 0;
        if(databaseHelper.getRealm().isClosed())
            databaseHelper.setRealm();

        if(databaseHelper.getRealm().where(Book.class).findAll().size() > 0)
            return databaseHelper.getRealm().where(Book.class).max("mId").intValue() + 1;
        else
            return 1;
    }

    /**
     * Method for sending message to the BookListFragments if book was created or deleted
     * @param request request message
     * @param book book object
     */
    public void sendRequest(int request, Book book){
        /*Sends as many messages as BookListFragments at the ViewPager*/
        Intent intent = new Intent(BookListFragment.RECEIVER_INTENT_FILTER);
        intent.putExtra(BookListFragment.RECEIVER_REQUEST, request);
        intent.putExtra(BookListFragment.RECEIVER_BOOK, book);
        LocalBroadcastManager.getInstance(BookCaseApplication.getInstance()).sendBroadcast(intent);
    }
}
