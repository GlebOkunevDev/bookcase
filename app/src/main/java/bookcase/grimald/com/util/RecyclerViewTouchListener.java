package bookcase.grimald.com.util;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import bookcase.grimald.com.R;
import bookcase.grimald.com.interfaces.RecyclerViewClickListener;

/**
 * https://medium.com/@harivigneshjayapalan/android-recyclerview-implementing-single-item-click-and-long-press-part-ii-b43ef8cb6ad8#.cwk179vvb
 * http://www.androidtutorialshub.com/android-recyclerview-click-listener-tutorial/
 * Created by user on 28.09.2016.
 */

public class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener{

    private GestureDetector gestureDetector;
    private RecyclerViewClickListener clickListener;

    public RecyclerViewTouchListener(Context context, final RecyclerView recyclerView, final RecyclerViewClickListener clickListener) {
        this.clickListener = clickListener;
        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                /*View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                View image = recyclerView.findViewById(R.id.iv_book_cover);
                if (child != null && image != null && clickListener != null) {
                    int pos = recyclerView.getLayoutManager().getChildCount();
                    int poss = recyclerView.getChildAdapterPosition(child);
                    float child_height = child.getHeight();
                    float startX = image.getX();
                    float endX = startX + image.getWidth();
                    int cof;
                    if(poss % pos > 1)
                        cof = poss % pos;
                    else
                        cof = 0;
                    float startY = image.getY() + cof*child_height;
                    float endY = startY + image.getHeight();
                    if(startX <= e.getX() && e.getX() <= endX && startY <= e.getY() && e.getY() <= endY) {
                        Log.d("RecyclerViewTouchLis", "book_cover X");
                        Log.d("RecyclerViewTouchLis", "book_cover Y");
                        clickListener.onClick(image, recyclerView.getChildAdapterPosition(child));
                        return false;
                    }
                    return true;
                    *//*clickListener.onClick(image, recyclerView.getChildAdapterPosition(child));
                    Log.d("RecyclerViewTouchLis", "book_cover child != null && clickListener != null");
                    return false;*//*
                }else{
                    *//*clickListener.onClick(child, recyclerView.getChildAdapterPosition(child));*//*
                    return true;
                }*/
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if (child != null && clickListener != null) {
                    clickListener.onLongClick(child, recyclerView.getChildAdapterPosition(child));
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View child = rv.findChildViewUnder(e.getX(), e.getY());
        /*View image = rv.findViewById(R.id.iv_book_cover);*/
        if(child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            clickListener.onClick(child, rv.getChildAdapterPosition(child));
        /**
         *  http://blog.pawpaw.mobi/2015/03/onitemclicklistener-recyclerview.html
         *  dispatchTouchEvent
         *  https://github.com/lucasr/twoway-view/blob/master/core/src/main/java/org/lucasr/twowayview/ClickItemTouchListener.java
         *  https://android-arsenal.com/details/1/3752#!description
        */
        /*RecyclerView.ViewHolder viewHolder = rv.findViewHolderForAdapterPosition(rv.getChildAdapterPosition(child));
        *//*RecyclerView.ViewHolder viewHolder = rv.findContainingViewHolder(child);*//*
        assert viewHolder != null;
        viewHolder.getItemId();
        Log.d(getClass().getSimpleName(), String.valueOf(viewHolder.getItemId()));*/
        /*if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
            if(child.dispatchTouchEvent(e)) {
                Log.d(getClass().getSimpleName(), "dispatchTouchEvent");
                if (rv.getChildAdapterPosition(child) != RecyclerView.NO_POSITION) {
                    clickListener.onClick(child, rv.getChildAdapterPosition(child));
                    return true;
                }
            }*/
            /*Log.d(getClass().getSimpleName(), String.valueOf(child.getId()));
            if(child.getId() == R.id.iv_book_cover){
                Log.d(getClass().getSimpleName(), "child.getId() == R.id.iv_book_cover");
            }else */
               /* if(image != null) {
                    Log.d(getClass().getSimpleName(), "image != null");
                    clickListener.onClick(image, rv.getChildAdapterPosition(child));
                }else {
                    Log.d(getClass().getSimpleName(), "image == null");
                    clickListener.onClick(child, rv.getChildAdapterPosition(child));
                }*/
//            clickListener.onClick(child, rv.getChildAdapterPosition(child));
//        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}
