package bookcase.grimald.com.util;

import android.support.v4.app.Fragment;
import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import bookcase.grimald.com.R;
import bookcase.grimald.com.adapter.CustomRecyclerViewAdapter;
import bookcase.grimald.com.fragment.BookListFragment;
import bookcase.grimald.com.interfaces.ActionModeDeleteListener;
import bookcase.grimald.com.interfaces.ActionModeDestroyListener;
import bookcase.grimald.com.interfaces.ActionModeMoveListener;
import bookcase.grimald.com.interfaces.ActionModeShareListener;

/**
 * http://www.androhub.com/android-contextual-action-mode-over-toolbar/
 * Created by user on 11.10.2016.
 */

public class ToolbarActionModeCallback implements ActionMode.Callback {

    private CustomRecyclerViewAdapter mAdapter;

    private ActionModeDeleteListener mDeleteListener;
    private ActionModeDestroyListener mDestroyListener;
    private ActionModeMoveListener mMoveListener;
    private ActionModeShareListener mShareListener;

    public ToolbarActionModeCallback(CustomRecyclerViewAdapter mAdapter, BookListFragment fragment) {
        this.mAdapter = mAdapter;
        onAttachInterface(fragment);
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.menu_actionmode, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        menu.findItem(R.id.action_share).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.findItem(R.id.action_delete).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.findItem(R.id.action_move).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        if(mAdapter == null)    return false;

        switch (item.getItemId()) {
            case R.id.action_delete:
                if (mAdapter.getSelectedIds().size() != 0)
                    mDeleteListener.onActionModeDeleteClick(mAdapter, mode);
                return true;
            case R.id.action_move:
                if (mAdapter.getSelectedIds().size() != 0)
                    mMoveListener.onActionModeMoveBooks(mAdapter, mode);
                return true;
            case R.id.action_share:
                if (mAdapter.getSelectedIds().size() != 0)
                    mShareListener.onActionModeShareBooks(mAdapter, mode);
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        mAdapter.removeSelection();
        mDestroyListener.onDestroyActionMode(mode);
        mDeleteListener = null;
        mDestroyListener = null;
        mMoveListener = null;
        mShareListener = null;
    }

    private void onAttachInterface(Fragment fragment) {
        if (fragment instanceof ActionModeDeleteListener) {
            mDeleteListener = (ActionModeDeleteListener) fragment;
        }else{
            throw new IllegalStateException(
                    "Fragment must implement the ActionModeDeleteListener.");
        }

        if (fragment instanceof ActionModeDestroyListener) {
            mDestroyListener = (ActionModeDestroyListener) fragment;
        }else{
            throw new IllegalStateException(
                    "Fragment must implement the ActionModeDestroyListener.");
        }

        if (fragment instanceof ActionModeMoveListener) {
            mMoveListener = (ActionModeMoveListener) fragment;
        }else{
            throw new IllegalStateException(
                    "Fragment must implement the ActionModeMoveListener.");
        }

        if (fragment instanceof ActionModeShareListener) {
            mShareListener = (ActionModeShareListener) fragment;
        }else{
            throw new IllegalStateException(
                    "Fragment must implement the ActionModeShareListener.");
        }
    }
}
