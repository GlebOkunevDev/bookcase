package bookcase.grimald.com.util;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import bookcase.grimald.com.R;
import bookcase.grimald.com.adapter.CustomPagerAdapter;
import bookcase.grimald.com.fragment.BookListFragment;

/**
 * Created by user on 16.09.2016.
 */
public class TabLayoutUtil {

    /**
     * Method for creating custom PagerVew adapter
     * @param fragmentManager FragmentManager
     * @return adapter
     */
    public static CustomPagerAdapter setupViewPager(FragmentManager fragmentManager) {
        CustomPagerAdapter adapter = new CustomPagerAdapter(fragmentManager);
        addPages(adapter, 3);
        return adapter;
    }

    /**
     * Method for adding pages (Fragments) to PagerAdapter
     * @param adapter PagerAdapter
     * @param numberOfTabs number of pages which need to add
     */
    public static void addPages(CustomPagerAdapter adapter, int numberOfTabs){
        if(adapter == null) return;

        for(int i = 0; i < numberOfTabs; i++){
            adapter.add(BookListFragment.newInstance(i));
        }
    }

    /**
     * Method for creating znd initialization Tab custom view
     * @param tabIcon id icon for Tab
     * @param context Context
     * @return view
     */
    public static View getTabView(int tabIcon, Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        ImageView icon = (ImageView) view.findViewById(R.id.icon);
        icon.setBackground(ContextCompat.getDrawable(context, tabIcon));
        return view;
    }

    /**
     * Method for selecting and opening the Tab on the specified position
     * @param pos position which need selected
     * @param context Context
     * @param tabLayout TabLayout
     * @param tabsSelectIcons array with select icons ids
     */
    public static void selectTab(int pos, Context context, TabLayout tabLayout, int[] tabsSelectIcons){
        if(tabLayout == null)  return;
        if(tabsSelectIcons == null)    return;

        tabLayout.getTabAt(pos).getCustomView().setSelected(true);
        tabLayout.getTabAt(pos).getCustomView().findViewById(R.id.icon).setBackground(ContextCompat.getDrawable(context, tabsSelectIcons[pos]));
    }
}
