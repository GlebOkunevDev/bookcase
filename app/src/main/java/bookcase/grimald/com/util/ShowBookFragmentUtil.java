package bookcase.grimald.com.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import bookcase.grimald.com.R;
import bookcase.grimald.com.activity.BaseListActivity;
import bookcase.grimald.com.application.BookCaseApplication;
import bookcase.grimald.com.database.DatabaseHelper;
import bookcase.grimald.com.dialog.DatePickerFragment;
import bookcase.grimald.com.dialog.DescriptionDialogFragment;
import bookcase.grimald.com.model.Book;
import bookcase.grimald.com.model.TempBook;
import bookcase.grimald.com.service.ImageWorkService;
import io.realm.Realm;

/**
 * Class - manager for ShowBookFragment
 * Created by user on 07.10.2016.
 */

public class ShowBookFragmentUtil {

    public static final int REQUEST_TO_LIST_CREATE = 1;
    public static final int REQUEST_TO_LIST_DELETE = 2;
    /*public static final int REQUEST_TO_LIST_UPDATE = 3;*/

    public static void openBookDescriptionDialog(int request, TextView textView, String hint, String title, int maxLength, Fragment fragment){
        DescriptionDialogFragment dialogFragment = DescriptionDialogFragment.newInstance(textView.getText().toString(), hint, title, maxLength);
        dialogFragment.setTargetFragment(fragment, request);
        dialogFragment.show(fragment.getFragmentManager(), fragment.getResources().getString(R.string.tag_description_dialog));
    }

    public static void openDatePickerDialog(int request, int mId, Date date, Fragment fragment){
        Date calendar;
        if(mId != -1 && date != null){
            calendar = date;
        }else{
            calendar = Calendar.getInstance().getTime();
        }

        DatePickerFragment dialogFragment = DatePickerFragment.newInstance(calendar);
        dialogFragment.setTargetFragment(fragment, request);
        dialogFragment.show(fragment.getFragmentManager(), fragment.getResources().getString(R.string.tag_datepicker_dialog));
    }

    public static boolean checkResultCode(int resultCode, Intent data, TextView textView, int length, Context context, FloatingActionButton fab, View view){
        if (resultCode == DescriptionDialogFragment.SAVE_RESULT ) {
            textView.setText(data.getStringExtra(DescriptionDialogFragment.SAVE_NEW_DESCRIPTION));
        }
        if(resultCode == DescriptionDialogFragment.EXCEEDED_RESULT ){
            textView.setText(data.getStringExtra(DescriptionDialogFragment.SAVE_NEW_DESCRIPTION));
            createSnackBar(context.getResources().getString(R.string.snackbar_description)
                    +" "+ String.valueOf(data.getIntExtra(DescriptionDialogFragment.DESCRIPTION_TEXT_LENGTH, length)), view);
        }

        return changeFabIcon(R.drawable.ic_save_fab, true, fab, context);
    }

    public static boolean checkResultCode(int resultCode, Intent data, Button button, Context context, FloatingActionButton fab){
        if (resultCode == DatePickerFragment.SAVE_RESULT_DATEPICKER) {
            Calendar calendar = (Calendar) data.getSerializableExtra(DatePickerFragment.SAVE_START_END_READING_DATE);
            button.setText(DateFormat.format("dd.MM.yyyy", calendar).toString());
        }

        return changeFabIcon(R.drawable.ic_save_fab, true, fab, context);
    }

    public static void createSnackBar(String message, View view){
        if (view != null) {
            Snackbar.make(view, message, 2000).show();
        }
    }

    public static boolean changeFabIcon(int iconId, boolean changeStatus, FloatingActionButton mFab, Context context){
        mFab.setImageDrawable(ContextCompat.getDrawable(context, iconId));
        return changeStatus;
    }

    private static boolean validateBook(TempBook tempBook, int mStatus, Context context, View view){
        if(tempBook == null)   return false;

        if(TextUtils.isEmpty(tempBook.getName())){
            createSnackBar(context.getResources().getString(R.string.snackbar_empty_title), view);
            return false;
        }

        if (mStatus == 1 && tempBook.getStartReading() != null && tempBook.getEndReading() != null) {
            switch (tempBook.getStartReading().compareTo(tempBook.getEndReading())){
                case -1:
                    break;
                case 0:
                    //Даты никогда не равны, т.к. сравниваются в миллисекундах, включая время создания
                    createSnackBar(context.getResources().getString(R.string.snackbar_start_end_reading), view);
                    return false;
                case 1:
                    createSnackBar(context.getResources().getString(R.string.snackbar_start_end_reading), view);
                    return false;
            }
        }
        return  true;
    }

    public static TempBook bookSaveMethod(TempBook tempBook, Book book, int mStatus, int mId, Context context, View view){
        if(!validateBook(tempBook, mStatus, context, view)) {
            tempBook.setLastUpdate(null);
            return tempBook;
        }

        tempBook.setLastUpdate(Calendar.getInstance().getTime());

        if(mId == -1){
            BookListUtil.getInstance().createBook(addBookData(tempBook, book));
        }else{
            BookListUtil.getInstance().updateBook(addBookData(tempBook, book));
        }

        return tempBook;
    }

    /**
     * Method for copying data from the temporary book object into the book object if user changed it
     * @param tempBook the temporary book object
     * @param book the book object from DB
     * @return the book object
     */
    private static Book addBookData(final TempBook tempBook, final Book book){
        if(tempBook == null)    return null;
        if(book == null)    return null;

        Realm realm = DatabaseHelper.getInstance().getRealm();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                if(!tempBook.getName().equals(book.getName()))   book.setName(tempBook.getName());
                if(tempBook.getAuthor() != null && !tempBook.getAuthor().equals(book.getAuthor()))   book.setAuthor(tempBook.getAuthor());
                if(tempBook.getGenre() != null && !tempBook.getGenre().equals(book.getGenre()))   book.setGenre(tempBook.getGenre());
                if(tempBook.getDescription() != null && !tempBook.getDescription().equals(book.getDescription()))   book.setDescription(tempBook.getDescription());
                if(tempBook.getRecommendation() != null && !tempBook.getRecommendation().equals(book.getRecommendation()))   book.setRecommendation(tempBook.getRecommendation());
                if(tempBook.getLittleImagePath() != null && !tempBook.getLittleImagePath().equals(book.getLittleImagePath()))   book.setLittleImagePath(tempBook.getLittleImagePath());
                else if(tempBook.getLittleImagePath() == null) book.setLittleImagePath(tempBook.getLittleImagePath());
                if(tempBook.getBigImagePath() != null && !tempBook.getBigImagePath().equals(book.getBigImagePath()))   book.setBigImagePath(tempBook.getBigImagePath());
                else if(tempBook.getBigImagePath() == null) book.setBigImagePath(tempBook.getBigImagePath());
                if(tempBook.getStartReading() != null && !tempBook.getStartReading().equals(book.getStartReading()))   book.setStartReading(tempBook.getStartReading());
                if(tempBook.getEndReading() != null && !tempBook.getEndReading().equals(book.getEndReading()))   book.setEndReading(tempBook.getEndReading());
                if(tempBook.getLastUpdate() != null && !tempBook.getLastUpdate().equals(book.getLastUpdate()))   book.setLastUpdate(tempBook.getLastUpdate());
            }
        });

        return book;
    }

    /**
     * Method for copying data from the book object into the temporary book object
     * @param book the book object from DB
     * @param tempBook the temporary book object
     * @return the temporary book object
     */
    public static TempBook copyBook(Book book, TempBook tempBook){
        if(book == null) return null;
        if(tempBook == null) return null;

        tempBook.setId(book.getId());
        tempBook.setStatus(book.getStatus());
        if(!TextUtils.isEmpty(book.getName())) tempBook.setName(book.getName());
        if(!TextUtils.isEmpty(book.getAuthor())) tempBook.setAuthor(book.getAuthor());
        if(!TextUtils.isEmpty(book.getGenre())) tempBook.setGenre(book.getGenre());
        if(!TextUtils.isEmpty(book.getDescription())) tempBook.setDescription(book.getDescription());
        if(!TextUtils.isEmpty(book.getRecommendation())) tempBook.setRecommendation(book.getRecommendation());
        if(!TextUtils.isEmpty(book.getLittleImagePath())) tempBook.setLittleImagePath(book.getLittleImagePath());
        if(!TextUtils.isEmpty(book.getBigImagePath())) tempBook.setBigImagePath(book.getBigImagePath());
        if(book.getStartReading() != null) tempBook.setStartReading(book.getStartReading());
        if(book.getEndReading() != null) tempBook.setEndReading(book.getEndReading());
        if(book.getLastUpdate() != null) tempBook.setLastUpdate(book.getLastUpdate());
        tempBook.setBookCreateDate(book.getBookCreateDate());

        return tempBook;
    }

    public static void sendRequest(int request, Context context){
        Intent intent = new Intent(BaseListActivity.RECEIVER_INTENT_FILTER);
        intent.putExtra(BaseListActivity.RECEIVER_REQUEST, request);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private static boolean dialogDismissIfOk(Context context, DialogInterface dialog, final TempBook tempBook, final Book book,
                                          final int mStatus, final int mId, final Activity activity, final View view){
        if(dialog == null)  return false;

        TempBook tempSaveBook = bookSaveMethod(tempBook, book, mStatus, mId, activity, view);
        if (tempSaveBook.getLastUpdate() == null) return false;
        sendRequest(REQUEST_TO_LIST_CREATE, context);
        dialog.dismiss();
        return true;
    }

    /**
     * Method for starting ImageWorkService if a small image of the book cover must be deleted
     * @param context Context
     * @param tempBook the temporary book object
     * @param mId the book id
     * @param dialog DialogInterface
     */
    private static void deleteImage(final Context context, final TempBook tempBook, final int mId, DialogInterface dialog){
        if(dialog == null)  return;
        if(tempBook == null)    return;

        if(mId == -1 && !TextUtils.isEmpty(tempBook.getLittleImagePath()))
            startImageService(context, null, tempBook.getLittleImagePath(), null, ImageWorkService.DELETE_IMAGE);
        dialog.dismiss();
    }

    /**
     * Method of restoring(copying previous image) a small image of the book cover if the user has canceled the changes
     * @param context Context
     * @param book the book object from DB
     * @param tempBook the temporary book object
     */
    private static void returnLittleImage(Context context, Book book, TempBook tempBook){
        if(book == null)    return;

        if(!TextUtils.isEmpty(book.getBigImagePath())){
            startImageService(context, null, book.getBigImagePath(), tempBook, ImageWorkService.COPY_IMAGE);
            Picasso.with(context).invalidate(new File(BookCaseApplication.getInstance().getImagePath() + tempBook.getLittleImagePath()));
        }
    }

    public static void onBackPressedDialog(final Context context, final TempBook tempBook, final Book book, final int mStatus,
                                           final int mId, final Activity activity, final View view){
        if(tempBook == null)   return;
        if(book == null)   return;

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(activity.getResources().getString(R.string.alert_dialog_title_backpressed));
        builder.setPositiveButton(R.string.alert_dialog_backpressed_save, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(!dialogDismissIfOk(context, dialog, tempBook, book, mStatus, mId, activity, view))   return;
                activity.finish();
            }
        });
        builder.setNegativeButton(R.string.alert_dialog_backpressed_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                returnLittleImage(context, book, tempBook);
                deleteImage(context, tempBook, mId, dialog);
                activity.finish();
            }
        });
        builder.create().show();
    }

    public static void onNaveUtilsDialog(final Context context, final TempBook tempBook, final Book book, final int mStatus, final int mId,
                                         final Activity activity, final View view){
        if(tempBook == null)   return;
        if(book == null)   return;

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(activity.getResources().getString(R.string.alert_dialog_title_backpressed));
        builder.setPositiveButton(R.string.alert_dialog_backpressed_save, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(!dialogDismissIfOk(context, dialog, tempBook, book, mStatus, mId, activity, view))   return;
                NavUtils.navigateUpFromSameTask(activity);
            }
        });
        builder.setNegativeButton(R.string.alert_dialog_backpressed_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                returnLittleImage(context, book, tempBook);
                deleteImage(context, tempBook, mId, dialog);
                NavUtils.navigateUpFromSameTask(activity);
            }
        });
        builder.create().show();
    }

    public static void deleteBook(final Context context, final Book mBook, final Activity activity){
        if(mBook == null)   return;

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(activity.getResources().getString(R.string.alert_dialog_title_delete));
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.alert_dialog_delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Book book = new Book();
                book.setStatus(mBook.getStatus());
                if(!TextUtils.isEmpty(mBook.getLittleImagePath()))
                    startImageService(context, null, mBook.getLittleImagePath(), null, ImageWorkService.DELETE_IMAGE);
                BookListUtil.getInstance().deleteBook(mBook);
                sendRequest(REQUEST_TO_LIST_DELETE, context);
                BookListUtil.getInstance().sendRequest(BookListFragmentUtil.DELETE_BOOK, book);
                dialog.dismiss();
                activity.finish();
            }
        });
        builder.setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.dismiss();
            }
        });

        builder.create().show();
    }

    /**
     * Method for getting image path where it is stored and then start IntentService for coping and compressing this image
     * @param context Context
     * @param tempBook the temporary book object
     * @param data intent which contain data where image is stored
     * @param mProgressBar ProgressBar for showing that image is loading
     * @param mBookCover container for loading image
     * @return the temporary book object
     */
    public static TempBook getImage(Context context, TempBook tempBook, Intent data, final ProgressBar mProgressBar, ImageView mBookCover){
        if(tempBook == null)   return null;
        if(data == null)    return tempBook;
        if(mProgressBar == null)    return tempBook;
        String realPathNew = ImageUtil.getRealPath(data.getDataString(), context);
        if(realPathNew == null)    return tempBook;
        if(tempBook.getBigImagePath() != null && tempBook.getBigImagePath().equals(realPathNew))  return tempBook;

        tempBook.setBigImagePath(realPathNew);
        mBookCover.setImageResource(android.R.color.transparent);
        mProgressBar.setVisibility(View.VISIBLE);
        startImageService(context, data, null, tempBook, ImageWorkService.COPY_IMAGE);
        return tempBook;
    }

    /**
     * Method for updating image path after their copping to the app path
     * @param context Context
     * @param tempBook the temporary book object
     * @param mProgressBar ProgressBar for showing that image is loading
     * @param mBookCover container for loading image
     * @param path path where image is stored
     * @return the temporary book object
     */
    public static TempBook updateImagePath(Context context, TempBook tempBook, final ProgressBar mProgressBar, ImageView mBookCover, String path){
        if(tempBook == null)   return null;
        if(mProgressBar == null)    return tempBook;

        if(tempBook.getLittleImagePath() == null)
            tempBook.setLittleImagePath(path);

        Picasso.with(context).invalidate(new File(BookCaseApplication.getInstance().getImagePath() + path));
        loadImage(context, mProgressBar, mBookCover, path);
        return tempBook;
    }

    /**
     * Method for starting IntentService for copy or delete image not in the main thread
     * @param context Context
     * @param data intent which contain data where image is stored
     * @param path path where image is stored
     * @param mBook the temporary book object
     * @param action type of action what to do with image: copy or delete
     */
    public static void startImageService(Context context, Intent data, String path, TempBook mBook, int action){
        Intent imageService = new Intent(context, ImageWorkService.class);
        if(data != null && mBook != null){
            imageService.putExtra(ImageWorkService.IMAGE_PATH, data.getDataString());
            imageService.putExtra(ImageWorkService.BOOK_FOR_ACTION, mBook);
        } else if(mBook != null){
            imageService.putExtra(ImageWorkService.IMAGE_PATH, path);
            imageService.putExtra(ImageWorkService.BOOK_FOR_ACTION, mBook);
            /**@param isRealPath true if image path is real, false otherwise*/
            imageService.putExtra(ImageWorkService.PATH_CATEGORY, true/*isRealPath*/);
        }else{
            imageService.putExtra(ImageWorkService.IMAGE_PATH, path);
        }
        imageService.putExtra(ImageWorkService.ACTION_WITH_IMAGE, action);
        context.startService(imageService);
    }

    /**
     * Method for loading Book cover
     * @param context Context
     * @param mProgressBar ProgressBar for showing that image is loading
     * @param mBookCover container for loading image
     * @param imagePath path where image is stored
     */
    public static void loadImage(final Context context, final ProgressBar mProgressBar, ImageView mBookCover, String imagePath){
        if(mProgressBar == null)    return;
        if(mProgressBar.getVisibility() != View.VISIBLE )
            mProgressBar.setVisibility(View.VISIBLE);
        Picasso.with(context).load(new File(BookCaseApplication.getInstance().getImagePath() + imagePath))
                .error(R.drawable.book_cover_not_available)
                .fit().centerInside().into(mBookCover, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                Log.e(getClass().getSimpleName(), "Picasso image load error");
            }
        });
    }
}
